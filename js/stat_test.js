var stat = require('./stat.js');
var misc = require('./misc.js');

var mysql = require('mysql');
var TBA = require('thebluealliance');
var tba = new TBA('FRCScout2018','Software for scouting for 2018 season','1.1.1');

// let t = 2.132;
// let dof = 4;

// console.log(stat.tCDF(t, dof));

// let x1 = 4.5862;
// let s1 = 2.0597;
// let n1 = 9.6667;
// let x2 = 4.0400;
// let s2 = 1.8650;
// let n2 = 8.3333;

// console.log(stat.welchTTest(x1, s1, n1, x2, s2, n2));
// let t = stat.welchTScore(x1, s1, n1, x2, s2, n2);
// let v = stat.welchDOF(s1, n1, s2, n2);
// console.log(t);
// console.log(v);
// console.log(stat.tCDF(t, v));

var poolLocal = null;
var connectionLocal = null;

console.log("Connecting to local DB");
poolLocal = mysql.createPool({
    connectionLimit: 100,
    host     : '127.0.0.1',
    user     : 'root',
    password : '',
    database : 'frcscout2018',
    debug    : false
});
poolLocal.getConnection(function(err, connection) {
  if(err)
    console.error(err);
  else
    connectionLocal = connection;
});


var x1 = 0, s1 = 0, n1 = 0, x2 = 0, s2 = 0, n2 = 0;
var crossX1, crossS1, climbX1, climbS1, plusX1, plusS1, exchX1, exchS1;
var crossX2, crossS2, climbX2, climbS2, plusX2, plusS2, exchX2, exchS2;
var auto_prob1, auto_prob2, climb_prob1, climb_prob2;
var correct_count = 0;
var total_err = 0;
var tot_rp_err = 0;
var num_matches = 0;

var teams_rp = new Array();

function loopMatches(matches, x, callback)
{
  if (matches[x].comp_level == 'qm')
  {
    num_matches++;
    // console.log(matches[x]);
    console.log('match:', matches[x].match_number);
    var blue_teams = matches[x].alliances.blue.teams;
    var red_teams = matches[x].alliances.red.teams;
    console.log('blue:', blue_teams);
    console.log('red:', red_teams);
    blue_teams = [ blue_teams[0].slice(3), blue_teams[1].slice(3), blue_teams[2].slice(3) ];
    red_teams = [ red_teams[0].slice(3), red_teams[1].slice(3), red_teams[2].slice(3) ];
    
    var blue_query = "SELECT AVG(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'avg', STDDEV(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'stdev', " +
    "COUNT(tele_scale_high_made)/3 AS 'size', AVG(auto_cross) AS 'cross_avg', STDDEV(auto_cross) AS 'cross_std', AVG(tele_climb/(tele_climb_failed+tele_climb)) AS 'climb_avg', STDDEV(tele_climb/(tele_climb_failed+tele_climb)) as 'climb_std', AVG(tele_plus_one/(tele_plus_one+tele_plus_one_failed)) AS 'plus_avg', STDDEV(tele_plus_one/(tele_plus_one+tele_plus_one_failed)) AS 'plus_std', 3*AVG(tele_exchange_made) AS 'exch_avg', STDDEV(tele_exchange_made) AS 'exch_std' FROM matches WHERE team_num=" + blue_teams[0] + " OR team_num=" + 
      blue_teams[1] + " OR team_num=" + blue_teams[2];
    var red_query = "SELECT AVG(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'avg', STDDEV(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'stdev', " +
    "COUNT(tele_scale_high_made)/3 AS 'size', AVG(auto_cross) AS 'cross_avg', STDDEV(auto_cross) AS 'cross_std', AVG(tele_climb/(tele_climb_failed+tele_climb)) AS 'climb_avg', STDDEV(tele_climb/(tele_climb_failed+tele_climb)) as 'climb_std', AVG(tele_plus_one/(tele_plus_one+tele_plus_one_failed)) AS 'plus_avg', STDDEV(tele_plus_one/(tele_plus_one+tele_plus_one_failed)) AS 'plus_std', 3*AVG(tele_exchange_made) AS 'exch_avg', STDDEV(tele_exchange_made) AS 'exch_std' FROM matches WHERE team_num=" + red_teams[0] + " OR team_num=" + 
      red_teams[1] + " OR team_num=" + red_teams[2];
    connectionLocal.query(blue_query, (err, rows) => {
      if(err) console.error(err);
      x1 = rows[0].avg;
      s1 = rows[0].stdev;
      n1 = rows[0].size;
      // console.log('blue:', rows);

      crossX1 = rows[0].cross_avg;
      crossS1 = rows[0].cross_std;
      climbX1 = rows[0].climb_avg;
      climbS1 = rows[0].climb_std;
      plusX1 = rows[0].plus_avg;
      plusS1 = rows[0].plus_std;
      exchX1 = rows[0].exch_avg;
      exchS1 = rows[0].exch_std;

      connectionLocal.query(red_query, (err, rows) => {
        if(err) console.error(err);
        var blue_won = matches[x].alliances.blue.score > matches[x].alliances.red.score;
        x2 = rows[0].avg;
        s2 = rows[0].stdev;
        n2 = rows[0].size;
        // console.log('red:', rows);

        crossX2 = rows[0].cross_avg;
        crossS2 = rows[0].cross_std;
        climbX2 = rows[0].climb_avg;
        climbS2 = rows[0].climb_std;
        plusX2 = rows[0].plus_avg;
        plusS2 = rows[0].plus_std;
        exchX2 = rows[0].exch_avg;
        exchS2 = rows[0].exch_std;

        // console.log(x1, s1, n1, x2, s2, n2);
        var pred_win = stat.welchTTest(x1, s1, n1, x2, s2, n2);
        total_err += blue_won - pred_win;
        console.log('blue won:', pred_win, blue_won);
        if ((pred_win > 0.50) == blue_won)
        {
          correct_count++;
        }

        // if (crossS1 == 0) crossS1 += ZERO;
        // if (crossS2 == 0) crossS2 += ZERO;
        auto_prob1 = Math.pow(crossX1, 3);//1 - stat.normCDF(3, crossX1, crossS1);
        auto_prob2 = Math.pow(crossX2, 3);//1 - stat.normCDF(3, crossX2, crossS2);

        climb_prob1 = climbX1 * plusX1 * (1 - stat.normCDF(3, exchX1, exchS1));
        climb_prob2 = climbX2 * plusX2 * (1 - stat.normCDF(3, exchX2, exchS2));
        // climb_prob1 = (1 - stat.normCDF(1, climbX1, climbS1)) * ((1 - stat.normCDF(1, plusX1, plusS1)) * (1 - stat.normCDF(3, exchX1, exchS1)));// + stat.normCDF(2, plusX1, plusS1));
        // climb_prob2 = (1 - stat.normCDF(1, climbX2, climbS2)) * ((1 - stat.normCDF(1, plusX2, plusS2)) * (1 - stat.normCDF(3, exchX2, exchS2)));// + stat.normCDF(2, plusX2, plusS2));

        var blue_rp = (pred_win > 0.5) * 2 + auto_prob1 + climb_prob1;
        var red_rp = (pred_win < 0.5) * 2 + auto_prob2 + climb_prob2;
        console.log('blue rp:', (pred_win > 0.5) * 2.0, auto_prob1, climb_prob1, blue_rp);
        console.log('red rp:', (pred_win < 0.5) * 2.0, auto_prob2, climb_prob2, red_rp);

        for (var t in blue_teams)
        {
          teams_rp[blue_teams[t]].rp += blue_rp;
        }
        for (var t in red_teams)
        {
          teams_rp[red_teams[t]].rp += red_rp;
        }
        
        if (++x < matches.length) loopMatches(matches, x, callback);
        else
        {
          callback();
        }
      });
    });
  }
  else
  {
    if (++x < matches.length) loopMatches(matches, x, callback);
    else
    {
      callback();
    }
  }
}

tba.getEventMatches('iri', 2018, (err, matches) => {
  if (err) console.error(err);
  var teams_query = "SELECT team_num FROM teams";
  connectionLocal.query(teams_query, (err, rows) => {
    if (err) console.error(err);
    for (var x in rows)
    {
      teams_rp[rows[x].team_num] = { team: rows[x].team_num, rp: 0 };
      // teams_rp[rows[x].team_num].team_num = rows[x].team_num;
      // teams_rp[rows[x].team_num].rp = 0;
    }
    
    loopMatches(matches, 0, () => {
      // console.log('RP scores:', teams_rp);

      
      misc.callTBA('/event/2018iri/rankings', 'YcKdUckYbs0ehPkAERDWlXkxXLn49nuIAhQ4gsfQb633i1vQyuS2sYCBpIsGvTPo').then((rankings) => {
        rankings = rankings.rankings;
        for (var x in rankings)
        {
          var rp_err = rankings[x].sort_orders[0] * rankings[x].matches_played - teams_rp[rankings[x].team_key.slice(3)].rp;
          console.log(rankings[x].team_key.slice(3), teams_rp[rankings[x].team_key.slice(3)].rp, rp_err);
          tot_rp_err += rp_err;
        }
        
        teams_rp.sort((a, b) => {
          return b.rp - a.rp;
        });
        
        console.log('win % err:', total_err, num_matches, total_err / num_matches);
        console.log('# correct:', correct_count, num_matches, correct_count / num_matches);
        console.log('rp err:', tot_rp_err, rankings.length, tot_rp_err / rankings.length);
      });
    });
  });
});