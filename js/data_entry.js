var team = require('./team.js')

function parseData(connectionLocal, req, callback)
{
  let team_num = Number(req.body.team_num);
  let match_num = Number(req.body.match_num);

  let auto_off_level_one = Number(req.body.auto_off_level_one);
  let auto_off_level_two = Number(req.body.auto_off_level_two);
  let auto_drive_mode = req.body.auto_drive_mode;
  let auto_start_pos = req.body.auto_start_pos;
  let auto_hatch_hp_intake_made = Number(req.body.auto_hatch_hp_intake_made);
  let auto_hatch_hp_intake_missed = Number(req.body.auto_hatch_hp_intake_missed);
  let auto_hatch_floor_intake_made = Number(req.body.auto_hatch_floor_intake_made);
  let auto_hatch_floor_intake_missed = Number(req.body.auto_hatch_floor_intake_missed);
  let auto_cargo_floor_intake_made = Number(req.body.auto_cargo_floor_intake_made);
  let auto_cargo_floor_intake_missed = Number(req.body.auto_cargo_floor_intake_missed);
  let auto_cargo_depot_intake_made = Number(req.body.auto_cargo_depot_intake_made);
  let auto_cargo_depot_intake_missed = Number(req.body.auto_cargo_depot_intake_missed);
  let auto_hatch_rocket_low_made = Number(req.body.auto_hatch_rocket_low_made);
  let auto_hatch_rocket_low_missed = Number(req.body.auto_hatch_rocket_low_missed);
  let auto_hatch_rocket_med_made = Number(req.body.auto_hatch_rocket_med_made);
  let auto_hatch_rocket_med_missed = Number(req.body.auto_hatch_rocket_med_missed);
  let auto_hatch_rocket_high_made = Number(req.body.auto_hatch_rocket_high_made);
  let auto_hatch_rocket_high_missed = Number(req.body.auto_hatch_rocket_high_missed);
  let auto_hatch_ship_made = Number(req.body.auto_hatch_ship_made);
  let auto_hatch_ship_missed = Number(req.body.auto_hatch_ship_missed);
  let auto_hatch_int_drops = Number(req.body.auto_hatch_int_drops);
  let auto_hatch_knockouts = Number(req.body.auto_hatch_knockouts);
  let auto_cargo_rocket_low_made = Number(req.body.auto_cargo_rocket_low_made);
  let auto_cargo_rocket_low_missed = Number(req.body.auto_cargo_rocket_low_missed);
  let auto_cargo_rocket_med_made = Number(req.body.auto_cargo_rocket_med_made);
  let auto_cargo_rocket_med_missed = Number(req.body.auto_cargo_rocket_med_missed);
  let auto_cargo_rocket_high_made = Number(req.body.auto_cargo_rocket_high_made);
  let auto_cargo_rocket_high_missed = Number(req.body.auto_cargo_rocket_high_missed);
  let auto_cargo_ship_made = Number(req.body.auto_cargo_ship_made);
  let auto_cargo_ship_missed = Number(req.body.auto_cargo_ship_missed);
  let auto_cargo_int_drops = Number(req.body.auto_cargo_int_drops);
  let auto_cargo_knockouts = Number(req.body.auto_cargo_knockouts);
  let tele_hatch_hp_intake_made = Number(req.body.tele_hatch_hp_intake_made);
  let tele_hatch_hp_intake_missed = Number(req.body.tele_hatch_hp_intake_missed);
  let tele_hatch_floor_intake_made = Number(req.body.tele_hatch_floor_intake_made);
  let tele_hatch_floor_intake_missed = Number(req.body.tele_hatch_floor_intake_missed);
  let tele_cargo_hp_intake_made = Number(req.body.tele_cargo_hp_intake_made);
  let tele_cargo_hp_intake_missed = Number(req.body.tele_cargo_hp_intake_missed);
  let tele_cargo_floor_intake_made = Number(req.body.tele_cargo_floor_intake_made);
  let tele_cargo_floor_intake_missed = Number(req.body.tele_cargo_floor_intake_missed);
  let tele_cargo_depot_intake_made = Number(req.body.tele_cargo_depot_intake_made);
  let tele_cargo_depot_intake_missed = Number(req.body.tele_cargo_depot_intake_missed);
  let tele_hatch_rocket_low_made = Number(req.body.tele_hatch_rocket_low_made);
  let tele_hatch_rocket_low_missed = Number(req.body.tele_hatch_rocket_low_missed);
  let tele_hatch_rocket_med_made = Number(req.body.tele_hatch_rocket_med_made);
  let tele_hatch_rocket_med_missed = Number(req.body.tele_hatch_rocket_med_missed);
  let tele_hatch_rocket_high_made = Number(req.body.tele_hatch_rocket_high_made);
  let tele_hatch_rocket_high_missed = Number(req.body.tele_hatch_rocket_high_missed);
  let tele_hatch_ship_made = Number(req.body.tele_hatch_ship_made);
  let tele_hatch_ship_missed = Number(req.body.tele_hatch_ship_missed);
  let tele_hatch_int_drops = Number(req.body.tele_hatch_int_drops);
  let tele_hatch_knockouts = Number(req.body.tele_hatch_knockouts);
  let tele_cargo_rocket_low_made = Number(req.body.tele_cargo_rocket_low_made);
  let tele_cargo_rocket_low_missed = Number(req.body.tele_cargo_rocket_low_missed);
  let tele_cargo_rocket_med_made = Number(req.body.tele_cargo_rocket_med_made);
  let tele_cargo_rocket_med_missed = Number(req.body.tele_cargo_rocket_med_missed);
  let tele_cargo_rocket_high_made = Number(req.body.tele_cargo_rocket_high_made);
  let tele_cargo_rocket_high_missed = Number(req.body.tele_cargo_rocket_high_missed);
  let tele_cargo_ship_made = Number(req.body.tele_cargo_ship_made);
  let tele_cargo_ship_missed = Number(req.body.tele_cargo_ship_missed);
  let tele_cargo_int_drops = Number(req.body.tele_cargo_int_drops);
  let tele_cargo_knockouts = Number(req.body.tele_cargo_knockouts);
  let tele_climb_level_one_made = Number(req.body.tele_climb_level_one_made);
  let tele_climb_level_one_missed = Number(req.body.tele_climb_level_one_missed);
  let tele_climb_level_two_made = Number(req.body.tele_climb_level_two_made);
  let tele_climb_level_two_missed = Number(req.body.tele_climb_level_two_missed);
  let tele_climb_level_three_made = Number(req.body.tele_climb_level_three_made);
  let tele_climb_level_three_missed = Number(req.body.tele_climb_level_three_missed);
  let tele_climb_start_time = Number(req.body.tele_climb_start_time);
  let tele_climb_elapsed_time = Number(req.body.tele_climb_elapsed_time);
  let tele_climb_assist_level_two_made = Number(req.body.tele_climb_assist_level_two_made);
  let tele_climb_assist_level_two_missed = Number(req.body.tele_climb_assist_level_two_missed);
  let tele_climb_assist_level_three_made = Number(req.body.tele_climb_assist_level_three_made);
  let tele_climb_assist_level_three_missed = Number(req.body.tele_climb_assist_level_three_missed);
  let tele_climb_assisted= Number(req.body.tele_climb_assisted);
  let tele_deaths = Number(req.body.tele_deaths);
  let driver_rating = Number(req.body.driver_rating);
  let defense_rating = Number(req.body.defense_rating);

  var put_data_sql = "INSERT INTO `matches` (`match_num`, `team_num`, `auto_off_level_one`, `auto_off_level_two`, `auto_drive_mode`, `auto_start_pos`, " +
  "`auto_hatch_hp_intake_made`, `auto_hatch_hp_intake_missed`, `auto_hatch_floor_intake_made`, `auto_hatch_floor_intake_missed`, " +
  "`auto_cargo_floor_intake_made`, `auto_cargo_floor_intake_missed`, `auto_cargo_depot_intake_made`, `auto_cargo_depot_intake_missed`, " +
  "`auto_hatch_rocket_low_made`, `auto_hatch_rocket_low_missed`, `auto_hatch_rocket_med_made`, `auto_hatch_rocket_med_missed`, " +
  "`auto_hatch_rocket_high_made`, `auto_hatch_rocket_high_missed`, `auto_hatch_ship_made`, `auto_hatch_ship_missed`, `auto_hatch_int_drops`, " +
  "`auto_hatch_knockouts`, `auto_cargo_rocket_low_made`, `auto_cargo_rocket_low_missed`, `auto_cargo_rocket_med_made`, " +
  "`auto_cargo_rocket_med_missed`, `auto_cargo_rocket_high_made`, `auto_cargo_rocket_high_missed`, `auto_cargo_ship_made`, " +
  "`auto_cargo_ship_missed`, `auto_cargo_int_drops`, `auto_cargo_knockouts`, `tele_hatch_hp_intake_made`, `tele_hatch_hp_intake_missed`, " +
  "`tele_hatch_floor_intake_made`, `tele_hatch_floor_intake_missed`, `tele_cargo_hp_intake_made`, `tele_cargo_hp_intake_missed`, " +
  "`tele_cargo_floor_intake_made`, `tele_cargo_floor_intake_missed`, `tele_cargo_depot_intake_made`, `tele_cargo_depot_intake_missed`, " +
  "`tele_hatch_rocket_low_made`, `tele_hatch_rocket_low_missed`, `tele_hatch_rocket_med_made`, `tele_hatch_rocket_med_missed`, " +
  "`tele_hatch_rocket_high_made`, `tele_hatch_rocket_high_missed`, `tele_hatch_ship_made`, `tele_hatch_ship_missed`, `tele_hatch_int_drops`, " +
  "`tele_hatch_knockouts`, `tele_cargo_rocket_low_made`, `tele_cargo_rocket_low_missed`, `tele_cargo_rocket_med_made`, " +
  "`tele_cargo_rocket_med_missed`, `tele_cargo_rocket_high_made`, `tele_cargo_rocket_high_missed`, `tele_cargo_ship_made`, " +
  "`tele_cargo_ship_missed`, `tele_cargo_int_drops`, `tele_cargo_knockouts`, `tele_climb_level_one_made`, `tele_climb_level_one_missed`, " +
  "`tele_climb_level_two_made`, `tele_climb_level_two_missed`, `tele_climb_level_three_made`, `tele_climb_level_three_missed`, " +
  "`tele_climb_start_time`, `tele_climb_elapsed_time`, `tele_climb_assist_level_two_made`, `tele_climb_assist_level_two_missed`, " +
  "`tele_climb_assist_level_three_made`, `tele_climb_assist_level_three_missed`, `tele_climb_assisted`, `tele_deaths`, `driver_rating`, `defense_rating`) " +
  "VALUES (" + match_num + ", " + team_num + ", " + auto_off_level_one + ", " + auto_off_level_two + ", '" + auto_drive_mode + "', '" + auto_start_pos + "', " +
  auto_hatch_hp_intake_made + ", " + auto_hatch_hp_intake_missed + ", " + auto_hatch_floor_intake_made + ", " + auto_hatch_floor_intake_missed + ", " + 
  auto_cargo_floor_intake_made + ", " + auto_cargo_floor_intake_missed + ", " + auto_cargo_depot_intake_made + ", " + auto_cargo_depot_intake_missed + ", " + 
  auto_hatch_rocket_low_made + ", " + auto_hatch_rocket_low_missed + ", " + auto_hatch_rocket_med_made + ", " + auto_hatch_rocket_med_missed + ", " + 
  auto_hatch_rocket_high_made + ", " + auto_hatch_rocket_high_missed + ", " + auto_hatch_ship_made + ", " + auto_hatch_ship_missed + ", " + 
  auto_hatch_int_drops + ", " + auto_hatch_knockouts + ", " + auto_cargo_rocket_low_made + ", " + auto_cargo_rocket_low_missed + ", " + 
  auto_cargo_rocket_med_made + ", " + auto_cargo_rocket_med_missed + ", " + auto_cargo_rocket_high_made + ", " + auto_cargo_rocket_high_missed + ", " + 
  auto_cargo_ship_made + ", " + auto_cargo_ship_missed + ", " + auto_cargo_int_drops + ", " + auto_cargo_knockouts + ", " + 
  tele_hatch_hp_intake_made + ", " + tele_hatch_hp_intake_missed + ", " + tele_hatch_floor_intake_made + ", " + tele_hatch_floor_intake_missed + ", " + 
  tele_cargo_hp_intake_made + ", " + tele_cargo_hp_intake_missed + ", " + tele_cargo_floor_intake_made + ", " + tele_cargo_floor_intake_missed + ", " + 
  tele_cargo_depot_intake_made + ", " + tele_cargo_depot_intake_missed + ", " + tele_hatch_rocket_low_made + ", " + tele_hatch_rocket_low_missed + ", " + 
  tele_hatch_rocket_med_made + ", " + tele_hatch_rocket_med_missed + ", " + tele_hatch_rocket_high_made + ", " + tele_hatch_rocket_high_missed + ", " + 
  tele_hatch_ship_made + ", " + tele_hatch_ship_missed + ", " + tele_hatch_int_drops + ", " + tele_hatch_knockouts + ", " + tele_cargo_rocket_low_made + ", " + 
  tele_cargo_rocket_low_missed + ", " + tele_cargo_rocket_med_made + ", " + tele_cargo_rocket_med_missed + ", " + tele_cargo_rocket_high_made + ", " +
  tele_cargo_rocket_high_missed + ", " + tele_cargo_ship_made + ", " + tele_cargo_ship_missed + ", " + tele_cargo_int_drops + ", " +
  tele_cargo_knockouts + ", " + tele_climb_level_one_made + ", " + tele_climb_level_one_missed + ", " + tele_climb_level_two_made + ", " +
  tele_climb_level_two_missed + ", " + tele_climb_level_three_made + ", " + tele_climb_level_three_missed + ", " + tele_climb_start_time + ", " +
  tele_climb_elapsed_time + ", " + tele_climb_assist_level_two_made + ", " + tele_climb_assist_level_two_missed + ", " + tele_climb_assist_level_three_made + ", " +
  tele_climb_assist_level_three_missed + ", " + tele_climb_assisted + ", " + tele_deaths + ", " + driver_rating + ", " + defense_rating + ");";

  var most_recent = -1;
  var most_recent_match = -1;
  var num_matches = 0;

  connectionLocal.query("SELECT * FROM matches WHERE match_num=" + match_num, function(err, rows) {
    if (err) console.error(err);
    num_matches = rows.length + 1 || 1;
    connectionLocal.query(put_data_sql, function(err, rows) {
      if(err) {
        most_recent = -1;
        most_recent_match = -1;
        team.updateTeams(connectionLocal, team_num, () => { callback(most_recent, most_recent_match, num_matches); });
        console.log(err);
      }
      else {
        most_recent = team_num;
        most_recent_match = match_num;
        team.updateTeams(connectionLocal, team_num, () => { callback(most_recent, most_recent_match, num_matches); });
      }
    });
  });
}

function parseDriver(connectionLocal, req, callback) {
  let team_num = Number(req.body.team_num);

  let robot_weight = Number(req.body.robot_weight);
  let robot_drivetrain = req.body.robot_drivetrain;

  let drv_under_defense_zero = Number(req.body.drv_under_defense_zero);
  let drv_under_defense_one = Number(req.body.drv_under_defense_one);
  let drv_under_defense_two = Number(req.body.drv_under_defense_two);
  let drv_under_defense_three = Number(req.body.drv_under_defense_three);
  let drv_under_defense_four = Number(req.body.drv_under_defense_four);
  let drv_under_defense_five = Number(req.body.drv_under_defense_five);
  let drv_under_defense_six = Number(req.body.drv_under_defense_six);
  let drv_under_defense_seven = Number(req.body.drv_under_defense_seven);
  let drv_under_defense_eight = Number(req.body.drv_under_defense_eight);
  let drv_under_defense_nine = Number(req.body.drv_under_defense_nine);
  let drv_under_defense_ten = Number(req.body.drv_under_defense_ten);
  let drv_under_defense = drv_under_defense_zero + ", " + drv_under_defense_one + ", " + drv_under_defense_two + ", " + 
      drv_under_defense_three + ", " + drv_under_defense_four + ", " + drv_under_defense_five + ", " + drv_under_defense_six + ", " + 
      drv_under_defense_seven + ", " + drv_under_defense_eight + ", " + drv_under_defense_nine + ", " + drv_under_defense_ten;

  let drv_without_defense_zero = Number(req.body.drv_without_defense_zero);
  let drv_without_defense_one = Number(req.body.drv_without_defense_one);
  let drv_without_defense_two = Number(req.body.drv_without_defense_two);
  let drv_without_defense_three = Number(req.body.drv_without_defense_three);
  let drv_without_defense_four = Number(req.body.drv_without_defense_four);
  let drv_without_defense_five = Number(req.body.drv_without_defense_five);
  let drv_without_defense_six = Number(req.body.drv_without_defense_six);
  let drv_without_defense_seven = Number(req.body.drv_without_defense_seven);
  let drv_without_defense_eight = Number(req.body.drv_without_defense_eight);
  let drv_without_defense_nine = Number(req.body.drv_without_defense_nine);
  let drv_without_defense_ten = Number(req.body.drv_without_defense_ten);
  let drv_without_defense = drv_without_defense_zero + ", " + drv_without_defense_one + ", " + drv_without_defense_two + ", " + 
      drv_without_defense_three + ", " + drv_without_defense_four + ", " + drv_without_defense_five + ", " + drv_without_defense_six + ", " + 
      drv_without_defense_seven + ", " + drv_without_defense_eight + ", " + drv_without_defense_nine + ", " + drv_without_defense_ten;

  let drv_playing_defense_zero = Number(req.body.drv_playing_defense_zero);
  let drv_playing_defense_one = Number(req.body.drv_playing_defense_one);
  let drv_playing_defense_two = Number(req.body.drv_playing_defense_two);
  let drv_playing_defense_three = Number(req.body.drv_playing_defense_three);
  let drv_playing_defense_four = Number(req.body.drv_playing_defense_four);
  let drv_playing_defense_five = Number(req.body.drv_playing_defense_five);
  let drv_playing_defense_six = Number(req.body.drv_playing_defense_six);
  let drv_playing_defense_seven = Number(req.body.drv_playing_defense_seven);
  let drv_playing_defense_eight = Number(req.body.drv_playing_defense_eight);
  let drv_playing_defense_nine = Number(req.body.drv_playing_defense_nine);
  let drv_playing_defense_ten = Number(req.body.drv_playing_defense_ten);
  let drv_playing_defense = drv_playing_defense_zero + ", " + drv_playing_defense_one + ", " + drv_playing_defense_two + ", " + 
      drv_playing_defense_three + ", " + drv_playing_defense_four + ", " + drv_playing_defense_five + ", " + drv_playing_defense_six + ", " + 
      drv_playing_defense_seven + ", " + drv_playing_defense_eight + ", " + drv_playing_defense_nine + ", " + drv_playing_defense_ten;

  let drv_overall = Number(drv_under_defense_zero + drv_without_defense_zero + drv_playing_defense_zero) + ", " + Number(drv_under_defense_one + drv_without_defense_one + drv_playing_defense_one) + ", " +
    Number(drv_under_defense_two + drv_without_defense_two + drv_playing_defense_two) + ", " + Number(drv_under_defense_three + drv_without_defense_three + drv_playing_defense_three) + ", " +
    Number(drv_under_defense_four + drv_without_defense_four + drv_playing_defense_four) + ", " + Number(drv_under_defense_five + drv_without_defense_five + drv_playing_defense_five) + ", " +
    Number(drv_under_defense_six + drv_without_defense_six + drv_playing_defense_six) + ", " + Number(drv_under_defense_seven + drv_without_defense_seven + drv_playing_defense_seven) + ", " +
    Number(drv_under_defense_eight + drv_without_defense_eight + drv_playing_defense_eight) + ", " + Number(drv_under_defense_nine + drv_without_defense_nine + drv_playing_defense_nine) + ", " +
    Number(drv_under_defense_ten + drv_without_defense_ten + drv_playing_defense_ten);

  let query;
  // console.log(robot_weight, robot_drivetrain);
  // console.log(robot_weight == 0 && robot_drivetrain == undefined);
  // console.log(drv_under_defense, drv_without_defense, drv_playing_defense);
  // console.log(drv_under_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0" && drv_without_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0" && drv_playing_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0");
  if (robot_weight == 0 && robot_drivetrain == undefined && drv_under_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0" && drv_without_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0" && drv_playing_defense == "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0") {
    console.log('neither dataset entered');
    query = "SELECT * FROM teams";
  }
  else if (robot_weight == 0 && robot_drivetrain == undefined) {
    console.log('only driver rating entered');
    query = "UPDATE teams SET tot_drv_under_defense='" + drv_under_defense + "', tot_drv_without_defense='" + drv_without_defense + 
      "', tot_drv_playing_defense='" + drv_playing_defense + "', tot_drv_overall='" + drv_overall + "' WHERE team_num=" + team_num;
  }
  else if (robot_drivetrain == undefined) {
    query = "UPDATE teams SET tot_drv_under_defense='" + drv_under_defense + "', tot_drv_without_defense='" + drv_without_defense + 
      "', tot_drv_playing_defense='" + drv_playing_defense + "', tot_drv_overall='" + drv_overall + "', robot_weight=" + robot_weight + " WHERE team_num=" + team_num;
  }
  else if (robot_weight == 0) {
    query = "UPDATE teams SET tot_drv_under_defense='" + drv_under_defense + "', tot_drv_without_defense='" + drv_without_defense + 
      "', tot_drv_playing_defense='" + drv_playing_defense + "', tot_drv_overall='" + drv_overall + "', robot_drivetrain='" + robot_drivetrain + "' WHERE team_num=" + team_num;
  }
  else {
    // console.log('everything entered');
    query = "UPDATE teams SET tot_drv_under_defense='" + drv_under_defense + "', tot_drv_without_defense='" + drv_without_defense + 
      "', tot_drv_playing_defense='" + drv_playing_defense + "',,tot_drv_overall='" + drv_overall + "', robot_weight=" + robot_weight + ", robot_drivetrain='" + robot_drivetrain + "' WHERE team_num=" + team_num;
  }
  connectionLocal.query(query, (err) => {
    if (err) console.error(err);
    callback();
  });
}

module.exports = {
  parseData: parseData,
  parseDriver: parseDriver
}