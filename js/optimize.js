let stat = require('./stat.js');

/*  
 *  DEFINE VAR ORDER AS: 
 *  0     Auto Off Level One
 *  1     Auto Off Level Two
 *  2     Hatch Rocket Low
 *  3     Hatch Rocket Medium
 *  4     Hatch Rocket High
 *  5     Hatch Cargo Ship Auto
 *  7     Hatch Cargo Ship Tele
 *  8     Cargo Rocket Low
 *  9     Cargo Rocket Medium
 *  0     Cargo Rocket High
 *  10     Cargo Cargo Ship
 *  11    Climb Level One
 *  12    Climb Level Two
 *  13    Climb Level Three
*/

function optimizeAllianceScore(connectionLocal, teams, percs, callback) {
  if (!percs) percs = [1, 1, 1];
  var means = new Array(14);
  var stdevs = new Array(14);
  for (var idx = 0; idx < 14; idx++) {
    means[idx] = new Array(3);
    stdevs[idx] = new Array(3);
  }
  addTeamData(connectionLocal, teams[0], percs[0], 0, means, stdevs, () => {
    addTeamData(connectionLocal, teams[1], percs[1], 1, means, stdevs, () => {
      addTeamData(connectionLocal, teams[2], percs[2], 2, means, stdevs, () => {
        for (var idx in means) {
          means[idx] = means[idx][0] + means[idx][1] + means[idx][2];
          stdevs[idx] = Math.sqrt(stdevs[idx][0] * stdevs[idx][1] + stdevs[idx][1] * stdevs[idx][2] + stdevs[idx][2] * stdevs[idx][2]);
        }
        for (var idx in means) {
          if (idx > 1 && idx < 10) {
            means[idx] = Math.max(scoringCap(means, idx), means[idx]);
          }
        }
        // console.log(means, expectedScore(means));
        callback(means, Math.round(1000*expectedScore(means))/1000);
      });
    });
  });
}

function boptimizeAllianceScore(connectionLocal, teams, percs, callback) {
  let dim = [2, 6, 10, 13];
  var means = new Array(dim[3]);
  var stdevs = new Array(dim[3]);

  for (var i = 0; i < dim[3]; i++) {
    means[i] = new Array(3);
    stdevs[i] = new Array(3);
  }

  addTeamData(connectionLocal, teams[0], percs[0], 0, means, stdevs, () => {
    addTeamData(connectionLocal, teams[1], percs[1], 1, means, stdevs, () => {
      addTeamData(connectionLocal, teams[2], percs[2], 2, means, stdevs, () => {
        for (var i = 0; i < dim[3]; i++) {
          means[i] = means[i][0] + means[i][1] + means[i][2];
          stdevs[i] = Math.sqrt(stdevs[i][0] * stdevs[i][1] + stdevs[i][1] * stdevs[i][2] + stdevs[i][2] * stdevs[i][2]);
        }
        // console.log(means, stdevs);

        let omega = 0.6, phiP = 0.6, phiG = 0.6;
        let particles = 200;

        var currentPos = new Array(particles);
        var bestKnownPos = new Array(particles);
        var velocity = new Array(particles);

        var ovrBestPos = new Array(dim[3]);


        for (var p = 0; p < particles; p++)
        {
          currentPos[p] = new Array(dim[3]);
          bestKnownPos[p] = new Array(dim[3]);
          velocity[p] = new Array(dim[3]);
          for (var d = 0; d < dim[3]; d++)
          {
            currentPos[p][d] = 0;
            bestKnownPos[p][d] = 0;
            velocity[p][d] = 0;
            ovrBestPos[d] = 0;
          }
        }

        // Auto optimize first
        for (var p = 0; p < particles; p++)
        {
          for (var d = 0; d < dim[0]; d++)
          {
            currentPos[p][d] = Math.random() * 3;
            bestKnownPos[p][d] = currentPos[p][d];
            velocity[p][d] = Math.random() * 6 - 3;
            ovrBestPos[d] = 0;
          }
          if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
          {
            // ovrBestPos = bestKnownPos[p];
            arrayCopy(ovrBestPos, bestKnownPos[p]);
          }
          // console.log('Pos', p, currentPos[p]);
          // console.log('Vel', p, velocity[p]);
        }

        for (var i = 0; i < 250; i++)
        {
          for (var p = 0; p < particles; p++)
          {
            for (var d = 0; d < dim[0]; d++)
            {
              let rp = Math.random();
              let rg = Math.random();
              velocity[p][d] = omega * velocity[p][d] + phiP * rp * (bestKnownPos[p][d] - currentPos[p][d]) + phiG * rg * (ovrBestPos[d] - currentPos[p][d]);
              currentPos[p][d] = Math.round(currentPos[p][d] + velocity[p][d]);
              if (currentPos[p][d] < 0 || currentPos[p][d] == -0) currentPos[p][d] = 0;
            }
            // console.log(velocity[p]);
            // console.log('cur', currentPos[p]);
            // console.log('est', bestKnownPos[p]);
            // console.log(expectedScore(currentPos[p], means, stdevs));

            if (expectedScore(currentPos[p], means, stdevs) > expectedScore(bestKnownPos[p], means, stdevs))
            {
              // bestKnownPos[p] = currentPos[p];
              arrayCopy(bestKnownPos[p], currentPos[p]);
              // console.log('bkp', bestKnownPos[p]);
              // console.log('bkp', expectedScore(bestKnownPos[p], means, stdevs));
              if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
              {
                // ovrBestPos = bestKnownPos[p];
                arrayCopy(ovrBestPos, bestKnownPos[p]);
                // console.log('auto', ovrBestPos);
                // console.log('auto', expectedScore(ovrBestPos, means, stdevs));
              }
            }
          }
        }
        // console.log('auto', ovrBestPos);
        // console.log('auto', expectedScore(ovrBestPos, means, stdevs));

        for (var p = 0; p < particles; p++)
        {
          for (var d = 0; d < dim[0]; d++)
          {
            currentPos[p][d] = ovrBestPos[d];
            bestKnownPos[p][d] = ovrBestPos[d];
            velocity[p][d] = 0;
          }
          for (var d = dim[0]; d < 6; d++)
          {
            currentPos[p][d] = Math.random() * 8;
            bestKnownPos[p][d] = currentPos[p][d];
            velocity[p][d] = Math.random() * 16 - 8;
            ovrBestPos[d] = 0;
          }
          if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
          {
            // ovrBestPos = bestKnownPos[p];
            arrayCopy(ovrBestPos, bestKnownPos[p]);
          }
          // console.log(currentPos[p]);
          // console.log(bestKnownPos[p]);
          // console.log('Pos', p, currentPos[p]);
          // console.log('Vel', p, velocity[p]);
        }

        for (var i = 0; i < 250; i++)
        {
          for (var p = 0; p < particles; p++)
          {
            for (var d = dim[0]; d < dim[1]; d++)
            {
              let rp = Math.random();
              let rg = Math.random();
              velocity[p][d] = omega * velocity[p][d] + phiP * rp * (bestKnownPos[p][d] - currentPos[p][d]) + phiG * rg * (ovrBestPos[d] - currentPos[p][d]);
              currentPos[p][d] = Math.round(currentPos[p][d] + velocity[p][d]);
              if (currentPos[p][d] < 0 || currentPos[p][d] == -0) currentPos[p][d] = 0;
            }
            // console.log(velocity[p]);
            // console.log(currentPos[p]);
            // console.log(bestKnownPos[p]);
            // console.log(expectedScore(currentPos[p], means, stdevs));
            // console.log(expectedScore(bestKnownPos[p], means, stdevs));

            if (expectedScore(currentPos[p], means, stdevs) > expectedScore(bestKnownPos[p], means, stdevs))
            {
              // bestKnownPos[p] = currentPos[p];
              arrayCopy(bestKnownPos[p], currentPos[p]);
              // console.log('bkp', bestKnownPos[p]);
              // console.log('bkp', expectedScore(bestKnownPos[p], means, stdevs));
              if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
              {
                // ovrBestPos = bestKnownPos[p];
                arrayCopy(ovrBestPos, bestKnownPos[p]);
                // console.log('hatch', ovrBestPos);
                // console.log('hatch', expectedScore(ovrBestPos, means, stdevs));
              }
            }
          }
        }
        // console.log('hatch', ovrBestPos);
        // console.log('hatch', expectedScore(ovrBestPos, means, stdevs));

        for (var p = 0; p < particles; p++)
        {
          for (var d = 0; d < dim[1]; d++)
          {
            currentPos[p][d] = ovrBestPos[d];
            bestKnownPos[p][d] = ovrBestPos[d];
            velocity[p][d] = 0;
          }
          for (var d = 6; d < 10; d++)
          {
            currentPos[p][d] = Math.random() * 8;
            bestKnownPos[p][d] = currentPos[p][d];
            velocity[p][d] = Math.random() * 16 - 8;
            ovrBestPos[d] = 0;
          }
          if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
          {
            // ovrBestPos = bestKnownPos[p];
            arrayCopy(ovrBestPos, bestKnownPos[p]);
          }
          // console.log(currentPos[p]);
          // console.log(bestKnownPos[p]);
          // console.log('Pos', p, currentPos[p]);
          // console.log('Vel', p, velocity[p]);
        }

        for (var i = 0; i < 250; i++)
        {
          for (var p = 0; p < particles; p++)
          {
            for (var d = dim[1]; d < dim[2]; d++)
            {
              let rp = Math.random();
              let rg = Math.random();
              velocity[p][d] = omega * velocity[p][d] + phiP * rp * (bestKnownPos[p][d] - currentPos[p][d]) + phiG * rg * (ovrBestPos[d] - currentPos[p][d]);
              currentPos[p][d] = Math.round(currentPos[p][d] + velocity[p][d]);
              if (currentPos[p][d] < 0 || currentPos[p][d] == -0) currentPos[p][d] = 0;
            }
            // console.log(velocity[p]);
            // console.log(currentPos[p]);
            // console.log(expectedScore(currentPos[p], means, stdevs));

            if (expectedScore(currentPos[p], means, stdevs) > expectedScore(bestKnownPos[p], means, stdevs))
            {
              // bestKnownPos[p] = currentPos[p];
              arrayCopy(bestKnownPos[p], currentPos[p]);
              // console.log('bkp', bestKnownPos[p]);
              // console.log('bkp', expectedScore(bestKnownPos[p], means, stdevs));
              if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
              {
                // ovrBestPos = bestKnownPos[p];
                arrayCopy(ovrBestPos, bestKnownPos[p]);
                // console.log('cargo', ovrBestPos);
                // console.log('cargo', expectedScore(ovrBestPos, means, stdevs));
              }
            }
          }
        }
        // console.log('cargo', ovrBestPos);
        // console.log('cargo', expectedScore(ovrBestPos, means, stdevs));

        for (var p = 0; p < particles; p++)
        {
          for (var d = 0; d < dim[2]; d++)
          {
            currentPos[p][d] = ovrBestPos[d];
            bestKnownPos[p][d] = ovrBestPos[d];
            velocity[p][d] = 0;
          }
          for (var d = dim[2]; d < dim[3]; d++)
          {
            currentPos[p][d] = Math.random() * 3;
            bestKnownPos[p][d] = currentPos[p][d];
            velocity[p][d] = Math.random() * 6 - 3;
            ovrBestPos[d] = 0;
          }
          if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
          {
            // ovrBestPos = bestKnownPos[p];
            arrayCopy(ovrBestPos, bestKnownPos[p]);
          }
          // console.log(currentPos[p]);
          // console.log(bestKnownPos[p]);
          // console.log('Pos', p, currentPos[p]);
          // console.log('Vel', p, velocity[p]);
        }

        for (var i = 0; i < 250; i++)
        {
          for (var p = 0; p < particles; p++)
          {
            for (var d = dim[2]; d < dim[3]; d++)
            {
              let rp = Math.random();
              let rg = Math.random();
              velocity[p][d] = omega * velocity[p][d] + phiP * rp * (bestKnownPos[p][d] - currentPos[p][d]) + phiG * rg * (ovrBestPos[d] - currentPos[p][d]);
              currentPos[p][d] = Math.round(currentPos[p][d] + velocity[p][d]);
              if (currentPos[p][d] < 0 || currentPos[p][d] == -0) currentPos[p][d] = 0;
            }
            // console.log(velocity[p]);
            // console.log(currentPos[p]);
            // console.log(expectedScore(currentPos[p], means, stdevs));

            if (expectedScore(currentPos[p], means, stdevs) > expectedScore(bestKnownPos[p], means, stdevs))
            {
              // bestKnownPos[p] = currentPos[p];
              arrayCopy(bestKnownPos[p], currentPos[p]);
              // console.log('bkp', bestKnownPos[p]);
              // console.log('bkp', expectedScore(bestKnownPos[p], means, stdevs));
              if (expectedScore(bestKnownPos[p], means, stdevs) > expectedScore(ovrBestPos, means, stdevs))
              {
                // bestClimbPos = bestKnownPos[p];
                arrayCopy(ovrBestPos, bestKnownPos[p]);
                // console.log('climb', ovrBestPos);
                // console.log('climb', expectedScore(ovrBestPos, means, stdevs));
              }
            }
          }
        }
        // console.log('climb', ovrBestPos);
        // console.log('climb', expectedScore(ovrBestPos, means, stdevs));

        // console.log('auto', bestAutoPos);
        // console.log('hatch', bestHatchPos);
        // console.log('cargo', bestCargoPos);
        // console.log('climb', bestClimbPos);

        // var ovrBestPos = bestAutoPos + bestHatchPos + bestCargoPos + bestClimbPos;

        // console.log('OBP', ovrBestPos);
        // console.log('OBP', expectedScore(ovrBestPos, means, stdevs));
        callback(ovrBestPos, expectedScore(ovrBestPos, means, stdevs));
      });
    });
  });
}

function addTeamData(connectionLocal, team_num, perc, index, means, stdevs, callback) {
  var query = "SELECT AVG(auto_off_level_one) AS 'AuOneAvg', STDDEV(auto_off_level_one) AS 'AuOneStd', " +
    "AVG(auto_off_level_two) AS 'AuTwoAvg', STDDEV(auto_off_level_two) AS 'AuTwoStd', " +
    "AVG(auto_hatch_rocket_low_made+tele_hatch_rocket_low_made) AS 'HaRoLoAvg', STDDEV(auto_hatch_rocket_low_made+tele_hatch_rocket_low_made) AS 'HaRoLoStd', " +
    "AVG(auto_hatch_rocket_med_made+tele_hatch_rocket_med_made) AS 'HaRoMeAvg', STDDEV(auto_hatch_rocket_low_made+tele_hatch_rocket_med_made) AS 'HaRoMeStd', " +
    "AVG(auto_hatch_rocket_high_made+tele_hatch_rocket_high_made) AS 'HaRoHiAvg', STDDEV(auto_hatch_rocket_low_made+tele_hatch_rocket_high_made) AS 'HaRoHiStd', " +
    "AVG(auto_hatch_ship_made) AS 'AuHaShAvg', STDDEV(auto_hatch_rocket_low_made) AS 'AuHaShStd', " +
    "AVG(tele_hatch_ship_made) AS 'TeHaShAvg', STDDEV(tele_hatch_rocket_low_made) AS 'TeHaShStd', " +
    "AVG(auto_cargo_rocket_low_made+tele_cargo_rocket_low_made) AS 'CaRoLoAvg', STDDEV(auto_cargo_rocket_low_made+tele_cargo_rocket_low_made) AS 'CaRoLoStd', " +
    "AVG(auto_cargo_rocket_med_made+tele_cargo_rocket_med_made) AS 'CaRoMeAvg', STDDEV(auto_cargo_rocket_med_made+tele_cargo_rocket_med_made) AS 'CaRoMeStd', " +
    "AVG(auto_cargo_rocket_high_made+tele_cargo_rocket_high_made) AS 'CaRoHiAvg', STDDEV(auto_cargo_rocket_high_made+tele_cargo_rocket_high_made) AS 'CaRoHiStd', " +
    "AVG(auto_cargo_ship_made+tele_cargo_ship_made) AS 'CaShAvg', STDDEV(auto_cargo_rocket_low_made+tele_cargo_ship_made) AS 'CaShStd', " +
    "AVG(tele_climb_level_one_made) AS 'ClOneAvg', STDDEV(tele_climb_level_one_made) AS 'ClOneStd', " +
    "AVG(tele_climb_leveL_two_made) AS 'ClTwoAvg', STDDEV(tele_climb_level_two_made) AS 'ClTwoStd', " +
    "AVG(tele_climb_leveL_three_made) AS 'ClThreeAvg', STDDEV(tele_climb_level_three_made) AS 'ClThreeStd' " +
    "FROM matches WHERE team_num=" + team_num;

  connectionLocal.query(query, (err, rows) => {
    if (err) console.error(err);
    means[0][index] = rows[0].AuOneAvg;
    stdevs[0][index] = rows[0].AuOneStd;
    means[1][index] = rows[0].AuTwoAvg;
    stdevs[1][index] = rows[0].AuTwoStd;
    means[2][index] = perc * rows[0].HaRoLoAvg;
    stdevs[2][index] = perc * rows[0].HaRoLoStd;
    means[3][index] = perc * rows[0].HaRoMeAvg;
    stdevs[3][index] = perc * rows[0].HaRoMeStd;
    means[4][index] = perc * rows[0].HaRoHiAvg;
    stdevs[4][index] = perc * rows[0].HaRoHiStd;
    means[5][index] = perc * rows[0].AuHaShAvg;
    stdevs[5][index] = perc * rows[0].AuHaShStd;
    means[6][index] = perc * rows[0].TeHaShAvg;
    stdevs[6][index] = perc * rows[0].TeHaShStd;
    means[7][index] = perc * rows[0].CaRoLoAvg;
    stdevs[7][index] = perc * rows[0].CaRoLoStd;
    means[8][index] = perc * rows[0].CaRoMeAvg;
    stdevs[8][index] = perc * rows[0].CaRoMeStd;
    means[9][index] = perc * rows[0].CaRoHiAvg;
    stdevs[9][index] = perc * rows[0].CaRoHiStd;
    means[10][index] = perc * rows[0].CaShAvg;
    stdevs[10][index] = perc * rows[0].CaShStd;
    means[11][index] = rows[0].ClOneAvg;
    stdevs[11][index] = rows[0].ClOneStd;
    means[12][index] = rows[0].ClTwoAvg;
    stdevs[12][index] = rows[0].ClTwoStd;
    means[13][index] = rows[0].ClThreeAvg;
    stdevs[13][index] = rows[0].ClThreeStd;
    // console.log(means);
    callback();
  });
}

function arrayCopy(a, b)
{
  for (var i in b)
  {
    a[i] = b[i];
  }
}

function expectedScore(particle)//, means, stdevs)
{
  var ret = 0;
  for (var i = 0; i < particle.length; i++)
  {
    ret += particle[i] * pointValue(i);// * adjustProb(particle, i, means[i], stdevs[i]);
  }
  return ret;
}

function adjustProb(particle, index, mean, stdev)
{
  var ret = 0;
  if (particle[index] > scoringCap(particle, index))
  {
    ret = 0;
  }
  else if (particle[index] < 0)
  {
    ret = 0;
  }
  else
  {
    if (stdev == 0) stdev = 0.00000001;
    var zScore = (particle[index] - mean) / stdev;
    // console.log('Z:', zScore);
    if (zScore < 0 || index == 0 || index == 10) // assume anyone can get off of/onto level one (liberal estimate)
    {
      ret = 1;
    }
    else
    {
      ret = 2*stat.normCDF(-zScore, 0, 1); // probability of being within plus/minus zScore
    }
  }
  return ret;
}

function scoringCap(particle, index)
{
  switch (index)
  {
    case 0: return Math.min(3, 3 - particle[1]);
    case 1: return Math.min(2, 3 - particle[0]);
    case 2: return 4;
    case 3: return 4;
    case 4: return 4;
    case 5: return 8; // ignoring null hatches - conservative estimate
    case 6: return Math.min(4, particle[2]);
    case 7: return Math.min(4, particle[3]);
    case 8: return Math.min(4, particle[4]);
    case 9: return Math.min(8, particle[5]); // ignoring preload balls - conservative estimate
    case 10: return Math.min(3, 3 - particle[11] - particle[12]);
    case 11: return Math.min(2, 3 - particle[10] - particle[12]);
    case 12: return Math.min(1, 3 - particle[10] - particle[11]); // could be argued to be 2 in some cases - conservative estimate
    default: return 0;
  }
}

function pointValue(index)
{
  switch (index)
  {
    case 0: return 3;
    case 1: return 6;
    case 2: return 2;
    case 3: return 2;
    case 4: return 2;
    case 5: return 5;
    case 6: return 2;
    case 7: return 3;
    case 8: return 3;
    case 9: return 3;
    case 10: return 3;
    case 11: return 3;
    case 12: return 6;
    case 13: return 12;
    default: return 0;
  }
}

module.exports = 
{
  optimizeAllianceScore: optimizeAllianceScore
}