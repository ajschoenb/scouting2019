let Q = require('q');

function getTeamData(connectionLocal, req, team_num, callback)
{
  var team_name = "";
  var num_matches = 0;
  var next_team_num = 0;
  var previous_team_num = 0;

  var tot_auto_off_level_one = 0;
  var tot_auto_off_level_two = 0;
  var tot_auto_mode_auton = 0;
  var tot_auto_mode_hybrid = 0;
  var tot_auto_start_left = 0;
  var tot_auto_start_center = 0;
  var tot_auto_start_right = 0;
  var tot_auto_hatch_hp_intake_made = 0;
  var tot_auto_hatch_hp_intake_attempts = 0;
  var tot_auto_hatch_floor_intake_made = 0;
  var tot_auto_hatch_floor_intake_attempts = 0;
  var tot_auto_cargo_floor_intake_made = 0;
  var tot_auto_cargo_floor_intake_attempts = 0;
  var tot_auto_cargo_depot_intake_made = 0;
  var tot_auto_cargo_depot_intake_attempts = 0;
  var tot_auto_hatch_rocket_low_made = 0;
  var tot_auto_hatch_rocket_low_attempts = 0;
  var tot_auto_hatch_rocket_med_made = 0;
  var tot_auto_hatch_rocket_med_attempts = 0;
  var tot_auto_hatch_rocket_high_made = 0;
  var tot_auto_hatch_rocket_high_attempts = 0;
  var tot_auto_hatch_ship_made = 0;
  var tot_auto_hatch_ship_attempts = 0;
  var tot_auto_hatch_int_drops = 0;
  var tot_auto_hatch_knockouts = 0;
  var tot_auto_cargo_rocket_low_made = 0;
  var tot_auto_cargo_rocket_low_attempts = 0;
  var tot_auto_cargo_rocket_med_made = 0;
  var tot_auto_cargo_rocket_med_attempts = 0;
  var tot_auto_cargo_rocket_high_made = 0;
  var tot_auto_cargo_rocket_high_attempts = 0;
  var tot_auto_cargo_ship_made = 0;
  var tot_auto_cargo_ship_attempts = 0;
  var tot_auto_cargo_int_drops = 0;
  var tot_auto_cargo_knockouts = 0;

  var avg_tele_hatch_hp_intake_made = 0;
  var avg_tele_hatch_hp_intake_attempts = 0;
  var avg_tele_hatch_floor_intake_made = 0;
  var avg_tele_hatch_floor_intake_attempts = 0;
  var avg_tele_cargo_hp_intake_made = 0;
  var avg_tele_cargo_hp_intake_attempts = 0;
  var avg_tele_cargo_floor_intake_made = 0;
  var avg_tele_cargo_floor_intake_attempts = 0;
  var avg_tele_cargo_depot_intake_made = 0;
  var avg_tele_cargo_depot_intake_attempts = 0;
  var avg_tele_hatch_rocket_low_made = 0;
  var avg_tele_hatch_rocket_low_attempts = 0;
  var avg_tele_hatch_rocket_med_made = 0;
  var avg_tele_hatch_rocket_med_attempts = 0;
  var avg_tele_hatch_rocket_high_made = 0;
  var avg_tele_hatch_rocket_high_attempts = 0;
  var avg_tele_hatch_ship_made = 0;
  var avg_tele_hatch_ship_attempts = 0;
  var avg_tele_hatch_int_drops = 0;
  var avg_tele_hatch_knockouts = 0;
  var avg_tele_cargo_rocket_low_made = 0;
  var avg_tele_cargo_rocket_low_attempts = 0;
  var avg_tele_cargo_rocket_med_made = 0;
  var avg_tele_cargo_rocket_med_attempts = 0;
  var avg_tele_cargo_rocket_high_made = 0;
  var avg_tele_cargo_rocket_high_attempts = 0;
  var avg_tele_cargo_ship_made = 0;
  var avg_tele_cargo_ship_attempts = 0;
  var avg_tele_cargo_int_drops = 0;
  var avg_tele_cargo_knockouts = 0;

  var tot_tele_climb_level_one_made = 0;
  var tot_tele_climb_level_one_attempts = 0;
  var tot_tele_climb_level_two_made = 0;
  var tot_tele_climb_level_two_attempts = 0;
  var tot_tele_climb_level_three_made = 0;
  var tot_tele_climb_level_three_attempts = 0;
  var avg_tele_climb_start_time = 0;
  var avg_tele_climb_elapsed_time = 0;
  var tot_tele_climb_assist_level_two_made = 0;
  var tot_tele_climb_assist_level_two_attempts = 0;
  var tot_tele_climb_assist_level_three_made = 0;
  var tot_tele_climb_assist_level_three_attempts = 0;
  var tot_tele_climb_assisted = 0;

  var tot_tele_deaths = 0;
  var avg_driver_rating = 0;
  var avg_defense_rating = 0;
  var avg_contributed_offense = 0;
  var avg_contributed_defense = 0;
  var avg_defense_points_lost = 0;
  var avg_net_contributed_points = 0;

  var avg_auto_hatch_made = 0;
  var avg_auto_cargo_made = 0;
  var avg_tele_hatch_made = 0;
  var avg_tele_cargo_made = 0;
  var avg_auto_cross_points = 0;
  var avg_tele_hatch_cargo_points = 0;
  var avg_tele_climb_points = 0;

  var tot_drv_under_defense = "";
  var tot_drv_without_defense = "";
  var tot_drv_playing_defense = "";
  var tot_drv_overall = "";
  var robot_weight = 0;
  var robot_drivetrain = "???";

  var hatch_hp_trend = "";
  var hatch_floor_trend = "";
  var cargo_hp_trend = "";
  var cargo_floor_trend = "";
  var cargo_depot_trend = "";
  var hatch_rocket_low_trend = "";
  var hatch_rocket_med_trend = "";
  var hatch_rocket_high_trend = "";
  var hatch_ship_trend = "";
  var hatch_comb_trend = "";
  var cargo_rocket_low_trend = "";
  var cargo_rocket_med_trend = "";
  var cargo_rocket_high_trend = "";
  var cargo_ship_trend = "";
  var cargo_comb_trend = "";

  var auto_desc_table = "";
  var trend_labels = "";
  var skip_render = false;
  var get_data = "SELECT * FROM teams WHERE team_num="+ team_num +"";
  var next_team = "SELECT * FROM teams WHERE team_num > "+ team_num +" ORDER BY team_num LIMIT 1";
  var previous_team = "SELECT * FROM teams WHERE team_num < "+ team_num +" ORDER BY team_num DESC LIMIT 1";
  var get_graph_data = "SELECT * FROM matches WHERE team_num="+ team_num +" ORDER BY match_num";

  updateTeams(connectionLocal, team_num, () => {
    connectionLocal.query(get_data, (err, rows) => {
      if(err || rows[0] === undefined) {
        skip_render = true;
        callback({});
        console.error('Returning to home page - bad team', team_num, 'entered');
      }
      else {
        team_name = rows[0].team_name;
        num_matches = rows[0].num_matches;
        tot_auto_off_level_one = rows[0].tot_auto_off_level_one;
        tot_auto_off_level_two = rows[0].tot_auto_off_level_two;
        tot_auto_mode_auton = rows[0].tot_auto_mode_auton;
        tot_auto_mode_hybrid = rows[0].tot_auto_mode_hybrid;
        tot_auto_start_left = rows[0].tot_auto_start_left;
        tot_auto_start_center = rows[0].tot_auto_start_center;
        tot_auto_start_right = rows[0].tot_auto_start_right;
        tot_auto_hatch_hp_intake_made = rows[0].tot_auto_hatch_hp_intake_made;
        tot_auto_hatch_hp_intake_attempts = rows[0].tot_auto_hatch_hp_intake_attempts;
        tot_auto_hatch_floor_intake_made = rows[0].tot_auto_hatch_floor_intake_made;
        tot_auto_hatch_floor_intake_attempts = rows[0].tot_auto_hatch_floor_intake_attempts;
        tot_auto_cargo_floor_intake_made = rows[0].tot_auto_cargo_floor_intake_made;
        tot_auto_cargo_floor_intake_attempts = rows[0].tot_auto_cargo_floor_intake_attempts;
        tot_auto_cargo_depot_intake_made = rows[0].tot_auto_cargo_depot_intake_made;
        tot_auto_cargo_depot_intake_attempts = rows[0].tot_auto_cargo_depot_intake_attempts;
        tot_auto_hatch_rocket_low_made = rows[0].tot_auto_hatch_rocket_low_made;
        tot_auto_hatch_rocket_low_attempts = rows[0].tot_auto_hatch_rocket_low_attempts;
        tot_auto_hatch_rocket_med_made = rows[0].tot_auto_hatch_rocket_med_made;
        tot_auto_hatch_rocket_med_attempts = rows[0].tot_auto_hatch_rocket_med_attempts;
        tot_auto_hatch_rocket_high_made = rows[0].tot_auto_hatch_rocket_high_made;
        tot_auto_hatch_rocket_high_attempts = rows[0].tot_auto_hatch_rocket_high_attempts;
        tot_auto_hatch_ship_made = rows[0].tot_auto_hatch_ship_made;
        tot_auto_hatch_ship_attempts = rows[0].tot_auto_hatch_ship_attempts;
        tot_auto_hatch_int_drops = rows[0].tot_auto_hatch_int_drops;
        tot_auto_hatch_knockouts = rows[0].tot_auto_hatch_knockouts;
        tot_auto_cargo_rocket_low_made = rows[0].tot_auto_cargo_rocket_low_made;
        tot_auto_cargo_rocket_low_attempts = rows[0].tot_auto_cargo_rocket_low_attempts;
        tot_auto_cargo_rocket_med_made = rows[0].tot_auto_cargo_rocket_med_made;
        tot_auto_cargo_rocket_med_attempts = rows[0].tot_auto_cargo_rocket_med_attempts;
        tot_auto_cargo_rocket_high_made = rows[0].tot_auto_cargo_rocket_high_made;
        tot_auto_cargo_rocket_high_attempts = rows[0].tot_auto_cargo_rocket_high_attempts;
        tot_auto_cargo_ship_made = rows[0].tot_auto_cargo_ship_made;
        tot_auto_cargo_ship_attempts = rows[0].tot_auto_cargo_ship_attempts;
        tot_auto_cargo_int_drops = rows[0].tot_auto_cargo_int_drops;
        tot_auto_cargo_knockouts = rows[0].tot_auto_cargo_knockouts;
      
        avg_tele_hatch_hp_intake_made = rows[0].avg_tele_hatch_hp_intake_made;
        avg_tele_hatch_hp_intake_attempts = rows[0].avg_tele_hatch_hp_intake_attempts;
        avg_tele_hatch_floor_intake_made = rows[0].avg_tele_hatch_floor_intake_made;
        avg_tele_hatch_floor_intake_attempts = rows[0].avg_tele_hatch_floor_intake_attempts;
        avg_tele_cargo_hp_intake_made = rows[0].avg_tele_cargo_hp_intake_made;
        avg_tele_cargo_hp_intake_attempts = rows[0].avg_tele_cargo_hp_intake_attempts;
        avg_tele_cargo_floor_intake_made = rows[0].avg_tele_cargo_floor_intake_made;
        avg_tele_cargo_floor_intake_attempts = rows[0].avg_tele_cargo_floor_intake_attempts;
        avg_tele_cargo_depot_intake_made = rows[0].avg_tele_cargo_depot_intake_made;
        avg_tele_cargo_depot_intake_attempts = rows[0].avg_tele_cargo_depot_intake_attempts;
        avg_tele_hatch_rocket_low_made = rows[0].avg_tele_hatch_rocket_low_made;
        avg_tele_hatch_rocket_low_attempts = rows[0].avg_tele_hatch_rocket_low_attempts;
        avg_tele_hatch_rocket_med_made = rows[0].avg_tele_hatch_rocket_med_made;
        avg_tele_hatch_rocket_med_attempts = rows[0].avg_tele_hatch_rocket_med_attempts;
        avg_tele_hatch_rocket_high_made = rows[0].avg_tele_hatch_rocket_high_made;
        avg_tele_hatch_rocket_high_attempts = rows[0].avg_tele_hatch_rocket_high_attempts;
        avg_tele_hatch_ship_made = rows[0].avg_tele_hatch_ship_made;
        avg_tele_hatch_ship_attempts = rows[0].avg_tele_hatch_ship_attempts;
        avg_tele_hatch_int_drops = rows[0].avg_tele_hatch_int_drops;
        avg_tele_hatch_knockouts = rows[0].avg_tele_hatch_knockouts;
        avg_tele_cargo_rocket_low_made = rows[0].avg_tele_cargo_rocket_low_made;
        avg_tele_cargo_rocket_low_attempts = rows[0].avg_tele_cargo_rocket_low_attempts;
        avg_tele_cargo_rocket_med_made = rows[0].avg_tele_cargo_rocket_med_made;
        avg_tele_cargo_rocket_med_attempts = rows[0].avg_tele_cargo_rocket_med_attempts;
        avg_tele_cargo_rocket_high_made = rows[0].avg_tele_cargo_rocket_high_made;
        avg_tele_cargo_rocket_high_attempts = rows[0].avg_tele_cargo_rocket_high_attempts;
        avg_tele_cargo_ship_made = rows[0].avg_tele_cargo_ship_made;
        avg_tele_cargo_ship_attempts = rows[0].avg_tele_cargo_ship_attempts;
        avg_tele_cargo_int_drops = rows[0].avg_tele_cargo_int_drops;
        avg_tele_cargo_knockouts = rows[0].avg_tele_cargo_knockouts;
      
        tot_tele_climb_level_one_made = rows[0].tot_tele_climb_level_one_made;
        tot_tele_climb_level_one_attempts = rows[0].tot_tele_climb_level_one_attempts;
        tot_tele_climb_level_two_made = rows[0].tot_tele_climb_level_two_made;
        tot_tele_climb_level_two_attempts = rows[0].tot_tele_climb_level_two_attempts;
        tot_tele_climb_level_three_made = rows[0].tot_tele_climb_level_three_made;
        tot_tele_climb_level_three_attempts = rows[0].tot_tele_climb_level_three_attempts;
        avg_tele_climb_start_time = rows[0].avg_tele_climb_start_time;
        avg_tele_climb_elapsed_time = rows[0].avg_tele_climb_elapsed_time;
        tot_tele_climb_assist_level_two_made = rows[0].tot_tele_climb_assist_level_two_made;
        tot_tele_climb_assist_level_two_attempts = rows[0].tot_tele_climb_assist_level_two_attempts;
        tot_tele_climb_assist_level_three_made = rows[0].tot_tele_climb_assist_level_three_made;
        tot_tele_climb_assist_level_three_attempts = rows[0].tot_tele_climb_assist_level_three_attempts;
        tot_tele_climb_assisted = rows[0].tot_tele_climb_assisted;
      
        tot_tele_deaths = rows[0].tot_tele_deaths;
        avg_driver_rating = rows[0].avg_driver_rating;
        avg_defense_rating = rows[0].avg_defense_rating;
        avg_contributed_offense = rows[0].avg_contributed_offense;
        avg_contributed_defense = rows[0].avg_contributed_defense;
        avg_defense_points_lost = rows[0].avg_defense_points_lost;
        avg_net_contributed_points = rows[0].avg_net_contributed_points;

        avg_auto_hatch_made = rows[0].avg_auto_hatch_made;
        avg_auto_cargo_made = rows[0].avg_auto_cargo_made;
        avg_tele_hatch_made = rows[0].avg_tele_hatch_made;
        avg_tele_cargo_made = rows[0].avg_tele_cargo_made;
        avg_auto_cross_points = rows[0].num_matches > 0 ? Math.round(1000*(3 * rows[0].tot_auto_off_level_one + 6 * rows[0].tot_auto_off_level_two) / rows[0].num_matches) / 1000 : 0; // fancy rounding ;)
        avg_tele_hatch_cargo_points = Math.round(1000*(3 * (avg_auto_cargo_made + avg_tele_cargo_made) + 2 * (avg_auto_hatch_made + avg_tele_hatch_made))) / 1000;
        avg_tele_climb_points = rows[0].avg_tele_climb_points;

        tot_drv_under_defense = rows[0].tot_drv_under_defense;
        tot_drv_without_defense = rows[0].tot_drv_without_defense;
        tot_drv_playing_defense = rows[0].tot_drv_playing_defense;
        tot_drv_overall = rows[0].tot_drv_overall;
        robot_weight = rows[0].robot_weight;
        robot_drivetrain = rows[0].robot_drivetrain || "???";

        var last_team;
        var first_team;
        var get_last_team_sql = "SELECT team_num FROM teams ORDER BY team_num DESC";
        var get_first_team_sql = "SELECT team_num FROM teams";
        connectionLocal.query(get_last_team_sql, (err, rows) => {
          if (err) console.error(err);
          last_team = rows[0].team_num;
          
          connectionLocal.query(get_first_team_sql, (err, rows) => {
            if (err) console.error(err);
            first_team = rows[0].team_num;
            
            connectionLocal.query(next_team, (err, rows) => {
              if (err) console.error(err);
              if(team_num == last_team)
                next_team_num = first_team;
              else
                next_team_num = rows[0].team_num;

              connectionLocal.query(previous_team, (err, rows) => {
                if (err) console.error(err);
                if(team_num == first_team)
                  previous_team_num = last_team;
                else
                  previous_team_num = rows[0].team_num;

                connectionLocal.query(get_graph_data, (err, rows) => {
                  if (err) console.error(err);
                  auto_desc_table = '<table class="table"><tr><th>Match #</th><th>Start Pos</th><th>Description</th></tr>';
                  for(var x in rows)
                  {
                    hatch_hp_trend += rows[x].tele_hatch_hp_intake_made + ", ";
                    hatch_floor_trend += rows[x].tele_hatch_floor_intake_made + ", ";
                    cargo_hp_trend += rows[x].tele_cargo_hp_intake_made + ", ";
                    cargo_floor_trend += rows[x].tele_cargo_floor_intake_made + ", ";
                    cargo_depot_trend += rows[x].tele_cargo_depot_intake_made + ", ";
                    hatch_rocket_low_trend += rows[x].tele_hatch_rocket_low_made + ", ";
                    hatch_rocket_med_trend += rows[x].tele_hatch_rocket_med_made + ", ";
                    hatch_rocket_high_trend += rows[x].tele_hatch_rocket_high_made + ", ";
                    hatch_ship_trend += rows[x].tele_hatch_ship_made + ", ";
                    hatch_comb_trend += Number(rows[x].tele_hatch_rocket_low_made + rows[x].tele_hatch_rocket_med_made + rows[x].tele_hatch_rocket_high_made + rows[x].tele_hatch_ship_made) + ", ";
                    cargo_rocket_low_trend += rows[x].tele_cargo_rocket_low_made + ", ";
                    cargo_rocket_med_trend += rows[x].tele_cargo_rocket_med_made + ", ";
                    cargo_rocket_high_trend += rows[x].tele_cargo_rocket_high_made + ", ";
                    cargo_ship_trend += rows[x].tele_cargo_ship_made + ", ";
                    cargo_comb_trend += Number(rows[x].tele_cargo_rocket_low_made + rows[x].tele_cargo_rocket_med_made + rows[x].tele_cargo_rocket_high_made + rows[x].tele_cargo_ship_made) + ", ";
                    trend_labels += rows[x].match_num + ", ";
                    
                    let auto_hatch_rocket_low_attempts = rows[x].auto_hatch_rocket_low_made + rows[x].auto_hatch_rocket_low_missed;
                    let auto_hatch_rocket_med_attempts = rows[x].auto_hatch_rocket_med_made + rows[x].auto_hatch_rocket_med_missed;
                    let auto_hatch_rocket_high_attempts = rows[x].auto_hatch_rocket_high_made + rows[x].auto_hatch_rocket_high_missed;
                    let auto_hatch_ship_attempts = rows[x].auto_hatch_ship_made + rows[x].auto_hatch_ship_missed;
                    let auto_cargo_rocket_low_attempts = rows[x].auto_cargo_rocket_low_made + rows[x].auto_cargo_rocket_low_missed;
                    let auto_cargo_rocket_med_attempts = rows[x].auto_cargo_rocket_med_made + rows[x].auto_cargo_rocket_med_missed;
                    let auto_cargo_rocket_high_attempts = rows[x].auto_cargo_rocket_high_made + rows[x].auto_cargo_rocket_high_missed;
                    let auto_cargo_ship_attempts = rows[x].auto_cargo_ship_made + rows[x].auto_cargo_ship_missed;

                    auto_desc_table += '<tr><td>' + rows[x].match_num + '</td><td>' + rows[x].auto_start_pos;
                    if (rows[x].auto_off_level_one > 0) auto_desc_table += ', Level One';
                    if (rows[x].auto_off_level_two > 0) auto_desc_table += ', Level Two';
                    auto_desc_table += '</td><td>';
                    if (auto_hatch_rocket_low_attempts > 0) auto_desc_table += 'H. Rocket Low: ' + rows[x].auto_hatch_rocket_low_made + ' / ' + auto_hatch_rocket_low_attempts + ', ';
                    if (auto_hatch_rocket_med_attempts > 0) auto_desc_table += 'H. Rocket Med: ' + rows[x].auto_hatch_rocket_med_made + ' / ' + auto_hatch_rocket_med_attempts + ', ';
                    if (auto_hatch_rocket_high_attempts > 0) auto_desc_table += 'H. Rocket High: ' + rows[x].auto_hatch_rocket_high_made + ' / ' + auto_hatch_rocket_high_attempts + ', ';
                    if (auto_hatch_ship_attempts > 0) auto_desc_table += 'H. Cargo Ship: ' + rows[x].auto_hatch_ship_made + ' / ' + auto_hatch_ship_attempts + ', ';
                    if (auto_cargo_rocket_low_attempts > 0) auto_desc_table += 'C. Rocket Low: ' + rows[x].auto_cargo_rocket_low_made + ' / ' + auto_cargo_rocket_low_attempts + ', ';
                    if (auto_cargo_rocket_med_attempts > 0) auto_desc_table += 'C. Rocket Med: ' + rows[x].auto_cargo_rocket_med_made + ' / ' + auto_cargo_rocket_med_attempts + ', ';
                    if (auto_cargo_rocket_high_attempts > 0) auto_desc_table += 'C. Rocket High: ' + rows[x].auto_cargo_rocket_high_made + ' / ' + auto_cargo_rocket_high_attempts + ', ';
                    if (auto_cargo_ship_attempts > 0) auto_desc_table += 'C. Cargo Ship: ' + rows[x].auto_cargo_ship_made + ' / ' + auto_cargo_ship_attempts + ', ';
                    if (auto_hatch_rocket_low_attempts + auto_hatch_rocket_med_attempts + auto_hatch_rocket_high_attempts + auto_hatch_ship_attempts + 
                        auto_cargo_rocket_low_attempts + auto_cargo_rocket_med_attempts + auto_cargo_rocket_high_attempts + auto_cargo_ship_attempts == 0)
                      auto_desc_table += 'Nothing</td></tr>';
                    else auto_desc_table = auto_desc_table.slice(0, auto_desc_table.length - 2) + '</td></tr>';
                  }
                  auto_desc_table += '</table>';

                  callback({
                    req: req,
                    skip_render: skip_render,
                    team_num: team_num,
                    team_name: team_name,
                    previous_team_num: previous_team_num,
                    next_team_num: next_team_num,
                    num_matches: num_matches,
                    tot_auto_off_level_one: tot_auto_off_level_one,
                    tot_auto_off_level_two: tot_auto_off_level_two,
                    tot_auto_mode_auton: tot_auto_mode_auton,
                    tot_auto_mode_hybrid: tot_auto_mode_hybrid,
                    tot_auto_start_left: tot_auto_start_left,
                    tot_auto_start_center: tot_auto_start_center,
                    tot_auto_start_right: tot_auto_start_right,
                    tot_auto_hatch_hp_intake_made: tot_auto_hatch_hp_intake_made,
                    tot_auto_hatch_hp_intake_attempts: tot_auto_hatch_hp_intake_attempts,
                    tot_auto_hatch_floor_intake_made: tot_auto_hatch_floor_intake_made,
                    tot_auto_hatch_floor_intake_attempts: tot_auto_hatch_floor_intake_attempts,
                    tot_auto_cargo_floor_intake_made: tot_auto_cargo_floor_intake_made,
                    tot_auto_cargo_floor_intake_attempts: tot_auto_cargo_floor_intake_attempts,
                    tot_auto_cargo_depot_intake_made: tot_auto_cargo_depot_intake_made,
                    tot_auto_cargo_depot_intake_attempts: tot_auto_cargo_depot_intake_attempts,
                    tot_auto_hatch_rocket_low_made: tot_auto_hatch_rocket_low_made,
                    tot_auto_hatch_rocket_low_attempts: tot_auto_hatch_rocket_low_attempts,
                    tot_auto_hatch_rocket_med_made: tot_auto_hatch_rocket_med_made,
                    tot_auto_hatch_rocket_med_attempts: tot_auto_hatch_rocket_med_attempts,
                    tot_auto_hatch_rocket_high_made: tot_auto_hatch_rocket_high_made,
                    tot_auto_hatch_rocket_high_attempts: tot_auto_hatch_rocket_high_attempts,
                    tot_auto_hatch_ship_made: tot_auto_hatch_ship_made,
                    tot_auto_hatch_ship_attempts: tot_auto_hatch_ship_attempts,
                    tot_auto_hatch_int_drops: tot_auto_hatch_int_drops,
                    tot_auto_hatch_knockouts: tot_auto_hatch_knockouts,
                    tot_auto_cargo_rocket_low_made: tot_auto_cargo_rocket_low_made,
                    tot_auto_cargo_rocket_low_attempts: tot_auto_cargo_rocket_low_attempts,
                    tot_auto_cargo_rocket_med_made: tot_auto_cargo_rocket_med_made,
                    tot_auto_cargo_rocket_med_attempts: tot_auto_cargo_rocket_med_attempts,
                    tot_auto_cargo_rocket_high_made: tot_auto_cargo_rocket_high_made,
                    tot_auto_cargo_rocket_high_attempts: tot_auto_cargo_rocket_high_attempts,
                    tot_auto_cargo_ship_made: tot_auto_cargo_ship_made,
                    tot_auto_cargo_ship_attempts: tot_auto_cargo_ship_attempts,
                    tot_auto_cargo_int_drops: tot_auto_cargo_int_drops,
                    tot_auto_cargo_knockouts: tot_auto_cargo_knockouts,
                  
                    avg_tele_hatch_hp_intake_made: avg_tele_hatch_hp_intake_made,
                    avg_tele_hatch_hp_intake_attempts: avg_tele_hatch_hp_intake_attempts,
                    avg_tele_hatch_floor_intake_made: avg_tele_hatch_floor_intake_made,
                    avg_tele_hatch_floor_intake_attempts: avg_tele_hatch_floor_intake_attempts,
                    avg_tele_cargo_hp_intake_made: avg_tele_cargo_hp_intake_made,
                    avg_tele_cargo_hp_intake_attempts: avg_tele_cargo_hp_intake_attempts,
                    avg_tele_cargo_floor_intake_made: avg_tele_cargo_floor_intake_made,
                    avg_tele_cargo_floor_intake_attempts: avg_tele_cargo_floor_intake_attempts,
                    avg_tele_cargo_depot_intake_made: avg_tele_cargo_depot_intake_made,
                    avg_tele_cargo_depot_intake_attempts: avg_tele_cargo_depot_intake_attempts,
                    avg_tele_hatch_rocket_low_made: avg_tele_hatch_rocket_low_made,
                    avg_tele_hatch_rocket_low_attempts: avg_tele_hatch_rocket_low_attempts,
                    avg_tele_hatch_rocket_med_made: avg_tele_hatch_rocket_med_made,
                    avg_tele_hatch_rocket_med_attempts: avg_tele_hatch_rocket_med_attempts,
                    avg_tele_hatch_rocket_high_made: avg_tele_hatch_rocket_high_made,
                    avg_tele_hatch_rocket_high_attempts: avg_tele_hatch_rocket_high_attempts,
                    avg_tele_hatch_ship_made: avg_tele_hatch_ship_made,
                    avg_tele_hatch_ship_attempts: avg_tele_hatch_ship_attempts,
                    avg_tele_hatch_int_drops: avg_tele_hatch_int_drops,
                    avg_tele_hatch_knockouts: avg_tele_hatch_knockouts,
                    avg_tele_cargo_rocket_low_made: avg_tele_cargo_rocket_low_made,
                    avg_tele_cargo_rocket_low_attempts: avg_tele_cargo_rocket_low_attempts,
                    avg_tele_cargo_rocket_med_made: avg_tele_cargo_rocket_med_made,
                    avg_tele_cargo_rocket_med_attempts: avg_tele_cargo_rocket_med_attempts,
                    avg_tele_cargo_rocket_high_made: avg_tele_cargo_rocket_high_made,
                    avg_tele_cargo_rocket_high_attempts: avg_tele_cargo_rocket_high_attempts,
                    avg_tele_cargo_ship_made: avg_tele_cargo_ship_made,
                    avg_tele_cargo_ship_attempts: avg_tele_cargo_ship_attempts,
                    avg_tele_cargo_int_drops: avg_tele_cargo_int_drops,
                    avg_tele_cargo_knockouts: avg_tele_cargo_knockouts,
                  
                    tot_tele_climb_level_one_made: tot_tele_climb_level_one_made,
                    tot_tele_climb_level_one_attempts: tot_tele_climb_level_one_attempts,
                    tot_tele_climb_level_two_made: tot_tele_climb_level_two_made,
                    tot_tele_climb_level_two_attempts: tot_tele_climb_level_two_attempts,
                    tot_tele_climb_level_three_made: tot_tele_climb_level_three_made,
                    tot_tele_climb_level_three_attempts: tot_tele_climb_level_three_attempts,
                    avg_tele_climb_start_time: avg_tele_climb_start_time,
                    avg_tele_climb_elapsed_time: avg_tele_climb_elapsed_time,
                    tot_tele_climb_assist_level_two_made: tot_tele_climb_assist_level_two_made,
                    tot_tele_climb_assist_level_two_attempts: tot_tele_climb_assist_level_two_attempts,
                    tot_tele_climb_assist_level_three_made: tot_tele_climb_assist_level_three_made,
                    tot_tele_climb_assist_level_three_attempts: tot_tele_climb_assist_level_three_attempts,
                    tot_tele_climb_assisted: tot_tele_climb_assisted,
                  
                    tot_tele_deaths: tot_tele_deaths,
                    avg_driver_rating: avg_driver_rating,
                    avg_defense_rating: avg_defense_rating,
                    avg_contributed_offense: avg_contributed_offense,
                    avg_contributed_defense: avg_contributed_defense,
                    avg_defense_points_lost: avg_defense_points_lost,
                    avg_net_contributed_points: avg_net_contributed_points,

                    avg_auto_hatch_made: avg_auto_hatch_made,
                    avg_auto_cargo_made: avg_auto_cargo_made,
                    avg_tele_hatch_made: avg_tele_hatch_made,
                    avg_tele_cargo_made: avg_tele_cargo_made,
                    avg_auto_cross_points: avg_auto_cross_points,
                    avg_tele_hatch_cargo_points: avg_tele_hatch_cargo_points,
                    avg_tele_climb_points: avg_tele_climb_points,

                    tot_drv_under_defense: tot_drv_under_defense,
                    tot_drv_without_defense: tot_drv_without_defense,
                    tot_drv_playing_defense: tot_drv_playing_defense,
                    tot_drv_overall: tot_drv_overall,
                    robot_weight: robot_weight,
                    robot_drivetrain: robot_drivetrain,
                  
                    hatch_hp_trend: hatch_hp_trend,
                    hatch_floor_trend: hatch_floor_trend,
                    cargo_hp_trend: cargo_hp_trend,
                    cargo_floor_trend: cargo_floor_trend,
                    cargo_depot_trend: cargo_depot_trend,
                    hatch_rocket_low_trend: hatch_rocket_low_trend,
                    hatch_rocket_med_trend: hatch_rocket_med_trend,
                    hatch_rocket_high_trend: hatch_rocket_high_trend,
                    hatch_ship_trend: hatch_ship_trend,
                    hatch_comb_trend: hatch_comb_trend,
                    cargo_rocket_low_trend: cargo_rocket_low_trend,
                    cargo_rocket_med_trend: cargo_rocket_med_trend,
                    cargo_rocket_high_trend: cargo_rocket_high_trend,
                    cargo_ship_trend: cargo_ship_trend,
                    cargo_comb_trend: cargo_comb_trend,
                    auto_desc_table: auto_desc_table,
                    trend_labels: trend_labels
                  });
                });
              });
            });
          });
        });
      }
    });
  });
}

function updateTeams(connectionLocal, team_num, callback) {
  // updateDefense(connectionLocal, team_num, () => {
    // console.log('Updating teams for', team_num);
    var team_sql = "UPDATE teams SET num_matches=(SELECT COUNT(*) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_off_level_one=(SELECT SUM(auto_off_level_one) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_off_level_one=(SELECT AVG(auto_off_level_one) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_off_level_two=(SELECT SUM(auto_off_level_two) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_off_level_two=(SELECT AVG(auto_off_level_two) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_auto_mode_auton=(SELECT COUNT(*) FROM matches WHERE auto_drive_mode=\"Auton\" AND team_num=" + team_num + "), " +
    "tot_auto_mode_hybrid=(SELECT COUNT(*) FROM matches WHERE auto_drive_mode=\"Hybrid\" AND team_num=" + team_num + "), " +
    
    "tot_auto_start_left=(SELECT COUNT(*) FROM matches WHERE auto_start_pos=\"Left\" AND team_num=" + team_num + "), " +
    "tot_auto_start_center=(SELECT COUNT(*) FROM matches WHERE auto_start_pos=\"Center\" AND team_num=" + team_num + "), " +
    "tot_auto_start_right=(SELECT COUNT(*) FROM matches WHERE auto_start_pos=\"Right\" AND team_num=" + team_num + "), " +

    "tot_auto_hatch_hp_intake_made=(SELECT SUM(auto_hatch_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_hp_intake_attempts=(SELECT SUM(auto_hatch_hp_intake_made+auto_hatch_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_hp_intake_made=(SELECT AVG(auto_hatch_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_hp_intake_attempts=(SELECT AVG(auto_hatch_hp_intake_made+auto_hatch_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_hp_intake_made=100*(tot_auto_hatch_hp_intake_made/tot_auto_hatch_hp_intake_attempts), " +

    "tot_auto_hatch_floor_intake_made=(SELECT SUM(auto_hatch_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_floor_intake_attempts=(SELECT SUM(auto_hatch_floor_intake_made+auto_hatch_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_floor_intake_made=(SELECT AVG(auto_hatch_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_floor_intake_attempts=(SELECT AVG(auto_hatch_floor_intake_made+auto_hatch_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_floor_intake_made=100*(tot_auto_hatch_floor_intake_made/tot_auto_hatch_floor_intake_attempts), " +

    "tot_auto_cargo_floor_intake_made=(SELECT SUM(auto_cargo_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_floor_intake_attempts=(SELECT SUM(auto_cargo_floor_intake_made+auto_cargo_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_floor_intake_made=(SELECT AVG(auto_cargo_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_floor_intake_attempts=(SELECT AVG(auto_cargo_floor_intake_made+auto_cargo_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_floor_intake_made=100*(tot_auto_cargo_floor_intake_made/tot_auto_cargo_floor_intake_attempts), " +

    "tot_auto_cargo_depot_intake_made=(SELECT SUM(auto_cargo_depot_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_depot_intake_attempts=(SELECT SUM(auto_cargo_depot_intake_made+auto_cargo_depot_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_depot_intake_made=(SELECT AVG(auto_cargo_depot_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_depot_intake_attempts=(SELECT AVG(auto_cargo_depot_intake_made+auto_cargo_depot_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_depot_intake_made=100*(tot_auto_cargo_depot_intake_made/tot_auto_cargo_depot_intake_attempts), " +

    "tot_auto_hatch_rocket_low_made=(SELECT SUM(auto_hatch_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_rocket_low_attempts=(SELECT SUM(auto_hatch_rocket_low_made+auto_hatch_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_low_made=(SELECT AVG(auto_hatch_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_low_attempts=(SELECT AVG(auto_hatch_rocket_low_made+auto_hatch_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_rocket_low_made=100*(tot_auto_hatch_rocket_low_made/tot_auto_hatch_rocket_low_attempts), " +
    
    "tot_auto_hatch_rocket_med_made=(SELECT SUM(auto_hatch_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_rocket_med_attempts=(SELECT SUM(auto_hatch_rocket_med_made+auto_hatch_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_med_made=(SELECT AVG(auto_hatch_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_med_attempts=(SELECT AVG(auto_hatch_rocket_med_made+auto_hatch_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_rocket_med_made=100*(tot_auto_hatch_rocket_med_made/tot_auto_hatch_rocket_med_attempts), " +

    "tot_auto_hatch_rocket_high_made=(SELECT SUM(auto_hatch_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_rocket_high_attempts=(SELECT SUM(auto_hatch_rocket_high_made+auto_hatch_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_high_made=(SELECT AVG(auto_hatch_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_rocket_high_attempts=(SELECT AVG(auto_hatch_rocket_high_made+auto_hatch_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_rocket_high_made=100*(tot_auto_hatch_rocket_high_made/tot_auto_hatch_rocket_high_attempts), " +

    "tot_auto_hatch_ship_made=(SELECT SUM(auto_hatch_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_hatch_ship_attempts=(SELECT SUM(auto_hatch_ship_made+auto_hatch_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_ship_made=(SELECT AVG(auto_hatch_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_ship_attempts=(SELECT AVG(auto_hatch_ship_made+auto_hatch_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_hatch_ship_made=100*(tot_auto_hatch_ship_made/tot_auto_hatch_ship_attempts), " +
    
    "tot_auto_hatch_int_drops=(SELECT SUM(auto_hatch_int_drops) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_int_drops=(SELECT AVG(auto_hatch_int_drops) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_auto_hatch_knockouts=(SELECT SUM(auto_hatch_knockouts) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_hatch_knockouts=(SELECT AVG(auto_hatch_knockouts) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_auto_cargo_rocket_low_made=(SELECT SUM(auto_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_rocket_low_attempts=(SELECT SUM(auto_cargo_rocket_low_made+auto_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_low_made=(SELECT AVG(auto_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_low_attempts=(SELECT AVG(auto_cargo_rocket_low_made+auto_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_rocket_low_made=100*(tot_auto_cargo_rocket_low_made/tot_auto_cargo_rocket_low_attempts), " +
    
    "tot_auto_cargo_rocket_med_made=(SELECT SUM(auto_cargo_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_rocket_med_attempts=(SELECT SUM(auto_cargo_rocket_med_made+auto_cargo_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_med_made=(SELECT AVG(auto_cargo_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_med_attempts=(SELECT AVG(auto_cargo_rocket_med_made+auto_cargo_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_rocket_med_made=100*(tot_auto_cargo_rocket_med_made/tot_auto_cargo_rocket_med_attempts), " +

    "tot_auto_cargo_rocket_high_made=(SELECT SUM(auto_cargo_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_rocket_high_attempts=(SELECT SUM(auto_cargo_rocket_high_made+auto_cargo_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_high_made=(SELECT AVG(auto_cargo_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_rocket_high_attempts=(SELECT AVG(auto_cargo_rocket_high_made+auto_cargo_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_rocket_high_made=100*(tot_auto_cargo_rocket_high_made/tot_auto_cargo_rocket_high_attempts), " +

    "tot_auto_cargo_ship_made=(SELECT SUM(auto_cargo_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_auto_cargo_ship_attempts=(SELECT SUM(auto_cargo_ship_made+auto_cargo_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_ship_made=(SELECT AVG(auto_cargo_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_ship_attempts=(SELECT AVG(auto_cargo_ship_made+auto_cargo_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_auto_cargo_ship_made=100*(tot_auto_cargo_ship_made/tot_auto_cargo_ship_attempts), " +
    
    "tot_auto_cargo_int_drops=(SELECT SUM(auto_cargo_int_drops) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_int_drops=(SELECT AVG(auto_cargo_int_drops) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_auto_cargo_knockouts=(SELECT SUM(auto_cargo_knockouts) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_auto_cargo_knockouts=(SELECT AVG(auto_cargo_knockouts) FROM matches WHERE team_num=" + team_num + "), " +

    
    "tot_tele_hatch_hp_intake_made=(SELECT SUM(tele_hatch_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_hp_intake_attempts=(SELECT SUM(tele_hatch_hp_intake_made+tele_hatch_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_hp_intake_made=(SELECT AVG(tele_hatch_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_hp_intake_attempts=(SELECT AVG(tele_hatch_hp_intake_made+tele_hatch_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_hp_intake_made=100*(tot_tele_hatch_hp_intake_made/tot_tele_hatch_hp_intake_attempts), " +

    "tot_tele_hatch_floor_intake_made=(SELECT SUM(tele_hatch_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_floor_intake_attempts=(SELECT SUM(tele_hatch_floor_intake_made+tele_hatch_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_floor_intake_made=(SELECT AVG(tele_hatch_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_floor_intake_attempts=(SELECT AVG(tele_hatch_floor_intake_made+tele_hatch_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_floor_intake_made=100*(tot_tele_hatch_floor_intake_made/tot_tele_hatch_floor_intake_attempts), " +

    "tot_tele_cargo_hp_intake_made=(SELECT SUM(tele_cargo_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_hp_intake_attempts=(SELECT SUM(tele_cargo_hp_intake_made+tele_cargo_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_hp_intake_made=(SELECT AVG(tele_cargo_hp_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_hp_intake_attempts=(SELECT AVG(tele_cargo_hp_intake_made+tele_cargo_hp_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_hp_intake_made=100*(tot_tele_cargo_hp_intake_made/tot_tele_cargo_hp_intake_attempts), " +

    "tot_tele_cargo_floor_intake_made=(SELECT SUM(tele_cargo_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_floor_intake_attempts=(SELECT SUM(tele_cargo_floor_intake_made+tele_cargo_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_floor_intake_made=(SELECT AVG(tele_cargo_floor_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_floor_intake_attempts=(SELECT AVG(tele_cargo_floor_intake_made+tele_cargo_floor_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_floor_intake_made=100*(tot_tele_cargo_floor_intake_made/tot_tele_cargo_floor_intake_attempts), " +

    "tot_tele_cargo_depot_intake_made=(SELECT SUM(tele_cargo_depot_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_depot_intake_attempts=(SELECT SUM(tele_cargo_depot_intake_made+tele_cargo_depot_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_depot_intake_made=(SELECT AVG(tele_cargo_depot_intake_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_depot_intake_attempts=(SELECT AVG(tele_cargo_depot_intake_made+tele_cargo_depot_intake_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_depot_intake_made=100*(tot_tele_cargo_depot_intake_made/tot_tele_cargo_depot_intake_attempts), " +

    "tot_tele_hatch_rocket_low_made=(SELECT SUM(tele_hatch_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_rocket_low_attempts=(SELECT SUM(tele_hatch_rocket_low_made+tele_hatch_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_low_made=(SELECT AVG(tele_hatch_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_low_attempts=(SELECT AVG(tele_hatch_rocket_low_made+tele_hatch_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_rocket_low_made=100*(tot_tele_hatch_rocket_low_made/tot_tele_hatch_rocket_low_attempts), " +
    
    "tot_tele_hatch_rocket_med_made=(SELECT SUM(tele_hatch_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_rocket_med_attempts=(SELECT SUM(tele_hatch_rocket_med_made+tele_hatch_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_med_made=(SELECT AVG(tele_hatch_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_med_attempts=(SELECT AVG(tele_hatch_rocket_med_made+tele_hatch_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_rocket_med_made=100*(tot_tele_hatch_rocket_med_made/tot_tele_hatch_rocket_med_attempts), " +

    "tot_tele_hatch_rocket_high_made=(SELECT SUM(tele_hatch_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_rocket_high_attempts=(SELECT SUM(tele_hatch_rocket_high_made+tele_hatch_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_high_made=(SELECT AVG(tele_hatch_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_rocket_high_attempts=(SELECT AVG(tele_hatch_rocket_high_made+tele_hatch_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_rocket_high_made=100*(tot_tele_hatch_rocket_high_made/tot_tele_hatch_rocket_high_attempts), " +

    "tot_tele_hatch_ship_made=(SELECT SUM(tele_hatch_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_hatch_ship_attempts=(SELECT SUM(tele_hatch_ship_made+tele_hatch_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_ship_made=(SELECT AVG(tele_hatch_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_ship_attempts=(SELECT AVG(tele_hatch_ship_made+tele_hatch_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_hatch_ship_made=100*(tot_tele_hatch_ship_made/tot_tele_hatch_ship_attempts), " +
    
    "tot_tele_hatch_int_drops=(SELECT SUM(tele_hatch_int_drops) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_int_drops=(SELECT AVG(tele_hatch_int_drops) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_tele_hatch_knockouts=(SELECT SUM(tele_hatch_knockouts) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_hatch_knockouts=(SELECT AVG(tele_hatch_knockouts) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_tele_cargo_rocket_low_made=(SELECT SUM(tele_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_rocket_low_attempts=(SELECT SUM(tele_cargo_rocket_low_made+tele_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_low_made=(SELECT AVG(tele_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_low_attempts=(SELECT AVG(tele_cargo_rocket_low_made+tele_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_rocket_low_made=100*(tot_tele_cargo_rocket_low_made/tot_tele_cargo_rocket_low_attempts), " +
    
    "tot_tele_cargo_rocket_med_made=(SELECT SUM(tele_cargo_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_rocket_med_attempts=(SELECT SUM(tele_cargo_rocket_med_made+tele_cargo_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_med_made=(SELECT AVG(tele_cargo_rocket_med_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_med_attempts=(SELECT AVG(tele_cargo_rocket_med_made+tele_cargo_rocket_med_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_rocket_med_made=100*(tot_tele_cargo_rocket_med_made/tot_tele_cargo_rocket_med_attempts), " +

    "tot_tele_cargo_rocket_high_made=(SELECT SUM(tele_cargo_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_rocket_high_attempts=(SELECT SUM(tele_cargo_rocket_high_made+tele_cargo_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_high_made=(SELECT AVG(tele_cargo_rocket_high_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_high_attempts=(SELECT AVG(tele_cargo_rocket_high_made+tele_cargo_rocket_high_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_rocket_high_made=100*(tot_tele_cargo_rocket_high_made/tot_tele_cargo_rocket_high_attempts), " +

    "tot_tele_cargo_ship_made=(SELECT SUM(tele_cargo_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_ship_attempts=(SELECT SUM(tele_cargo_ship_made+tele_cargo_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_ship_made=(SELECT AVG(tele_cargo_ship_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_ship_attempts=(SELECT AVG(tele_cargo_ship_made+tele_cargo_ship_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_ship_made=100*(tot_tele_cargo_ship_made/tot_tele_cargo_ship_attempts), " +
    
    "tot_tele_cargo_int_drops=(SELECT SUM(tele_cargo_int_drops) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_int_drops=(SELECT AVG(tele_cargo_int_drops) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_tele_cargo_knockouts=(SELECT SUM(tele_cargo_knockouts) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_knockouts=(SELECT AVG(tele_cargo_knockouts) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_tele_cargo_rocket_low_made=(SELECT SUM(tele_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_cargo_rocket_low_attempts=(SELECT SUM(tele_cargo_rocket_low_made+tele_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_low_made=(SELECT AVG(tele_cargo_rocket_low_made) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_cargo_rocket_low_attempts=(SELECT AVG(tele_cargo_rocket_low_made+tele_cargo_rocket_low_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_cargo_rocket_low_made=100*(tot_tele_cargo_rocket_low_made/tot_tele_cargo_rocket_low_attempts), " +

    
    "tot_tele_climb_level_one_made=(SELECT SUM(tele_climb_level_one_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_climb_level_one_attempts=(SELECT SUM(tele_climb_level_one_made+tele_climb_level_one_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_climb_level_one_made=100*(tot_tele_climb_level_one_made/tot_tele_climb_level_one_attempts), " +

    "tot_tele_climb_level_two_made=(SELECT SUM(tele_climb_level_two_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_climb_level_two_attempts=(SELECT SUM(tele_climb_level_two_made+tele_climb_level_two_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_climb_level_two_made=100*(tot_tele_climb_level_two_made/tot_tele_climb_level_two_attempts), " +

    "tot_tele_climb_level_three_made=(SELECT SUM(tele_climb_level_three_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_climb_level_three_attempts=(SELECT SUM(tele_climb_level_three_made+tele_climb_level_three_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_climb_level_three_made=100*(tot_tele_climb_level_three_attempts/tot_tele_climb_level_three_attempts), " +
    
    "avg_tele_climb_start_time=(SELECT AVG(tele_climb_start_time) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_climb_elapsed_time=(SELECT AVG(tele_climb_elapsed_time) FROM matches WHERE team_num=" + team_num + "), " +

    "tot_tele_climb_assist_level_two_made=(SELECT SUM(tele_climb_assist_level_two_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_climb_assist_level_two_attempts=(SELECT SUM(tele_climb_assist_level_two_made+tele_climb_assist_level_two_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_climb_assist_level_two_made=100*(tot_tele_climb_assist_level_two_made/tot_tele_climb_assist_level_two_attempts), " +
    
    "tot_tele_climb_assist_level_three_made=(SELECT SUM(tele_climb_assist_level_three_made) FROM matches WHERE team_num=" + team_num + "), " +
    "tot_tele_climb_assist_level_three_attempts=(SELECT SUM(tele_climb_assist_level_three_made+tele_climb_assist_level_three_missed) FROM matches WHERE team_num=" + team_num + "), " +
    "perc_tele_climb_assist_level_three_made=100*(tot_tele_climb_assist_level_three_made/tot_tele_climb_assist_level_three_attempts), " +
    

    "tot_auto_hatch_intakes=tot_auto_hatch_hp_intake_made+tot_auto_hatch_floor_intake_made, " +
    "avg_auto_hatch_intakes=avg_auto_hatch_hp_intake_made+avg_auto_hatch_floor_intake_made, " +

    "tot_auto_cargo_intakes=tot_auto_cargo_floor_intake_made+tot_auto_cargo_depot_intake_made, " +
    "avg_auto_cargo_intakes=avg_auto_cargo_floor_intake_made+avg_auto_cargo_depot_intake_made, " +
    
    "tot_auto_hatch_made=tot_auto_hatch_rocket_low_made+tot_auto_hatch_rocket_med_made+tot_auto_hatch_rocket_high_made+tot_auto_hatch_ship_made, " +
    "avg_auto_hatch_made=avg_auto_hatch_rocket_low_made+avg_auto_hatch_rocket_med_made+avg_auto_hatch_rocket_high_made+avg_auto_hatch_ship_made, " +

    "tot_auto_cargo_made=tot_auto_cargo_rocket_low_made+tot_auto_cargo_rocket_med_made+tot_auto_cargo_rocket_high_made+tot_auto_cargo_ship_made, " +
    "avg_auto_cargo_made=avg_auto_cargo_rocket_low_made+avg_auto_cargo_rocket_med_made+avg_auto_cargo_rocket_high_made+avg_auto_cargo_ship_made, " +
    
    "tot_tele_hatch_intakes=tot_tele_hatch_hp_intake_made+tot_tele_hatch_floor_intake_made, " +
    "avg_tele_hatch_intakes=avg_tele_hatch_hp_intake_made+avg_tele_hatch_floor_intake_made, " +

    "tot_tele_cargo_intakes=tot_tele_cargo_hp_intake_made+tot_tele_cargo_floor_intake_made+tot_tele_cargo_depot_intake_made, " +
    "avg_tele_cargo_intakes=avg_tele_cargo_hp_intake_made+avg_tele_cargo_floor_intake_made+avg_tele_cargo_depot_intake_made, " +
    
    "tot_tele_hatch_made=tot_tele_hatch_rocket_low_made+tot_tele_hatch_rocket_med_made+tot_tele_hatch_rocket_high_made+tot_tele_hatch_ship_made, " +
    "avg_tele_hatch_made=avg_tele_hatch_rocket_low_made+avg_tele_hatch_rocket_med_made+avg_tele_hatch_rocket_high_made+avg_tele_hatch_ship_made, " +

    "tot_tele_cargo_made=tot_tele_cargo_rocket_low_made+tot_tele_cargo_rocket_med_made+tot_tele_cargo_rocket_high_made+tot_tele_cargo_ship_made, " +
    "avg_tele_cargo_made=avg_tele_cargo_rocket_low_made+avg_tele_cargo_rocket_med_made+avg_tele_cargo_rocket_high_made+avg_tele_cargo_ship_made, " +

    "tot_tele_climb_points=3*tot_tele_climb_level_one_made+6*tot_tele_climb_level_two_made+12*tot_tele_climb_level_three_made+6*tot_tele_climb_assist_level_two_made+12*tot_tele_climb_assist_level_three_made, " +
    "avg_tele_climb_points=tot_tele_climb_points/num_matches, " +

    "tot_contributed_offense=3*tot_auto_off_level_one+6*tot_auto_off_level_two+2*(tot_auto_hatch_made+tot_tele_hatch_made)+3*tot_auto_hatch_ship_made+3*(tot_auto_cargo_made+tot_tele_cargo_made)+tot_tele_climb_points, " +
    "avg_contributed_offense=tot_contributed_offense/num_matches, " +

    "tot_net_contributed_points=tot_contributed_offense+tot_contributed_defense-tot_defense_points_lost, " +
    "avg_net_contributed_points=avg_contributed_offense+avg_contributed_defense-avg_defense_points_lost, " +

    "tot_tele_deaths=(SELECT SUM(tele_deaths) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_tele_deaths=(SELECT AVG(tele_deaths) FROM matches WHERE team_num=" + team_num + "), " +

    "avg_driver_rating=(SELECT AVG(driver_rating) FROM matches WHERE team_num=" + team_num + "), " +
    "avg_defense_rating=(SELECT AVG(defense_rating) FROM matches WHERE team_num=" + team_num + " AND defense_rating<>0) " +

    "WHERE team_num=" + team_num;

    connectionLocal.query(team_sql, function(err) {
      if (err) console.error(err);
      callback();
    });
  // });
}

function updateDefense(connectionLocal, team_num, callback)
{
  team_num = Number(team_num);
  // console.log("TODO: Updating defensive stats for", team_num);
  let get_matches = "SELECT * FROM matches WHERE team_num=" + team_num + " AND defense_rating>0";
  connectionLocal.query(get_matches, (err, rows) => {
    if (err) console.error(err);
    
    var match_promises = [];
    var tot_denied_pts = 0;
    var avg_denied_pts = 0;
    var count = 0;
    for (var x in rows)
    {
      let match_deferred = Q.defer();
      let get_match_data = "SELECT * FROM matches_tba WHERE match_num=" + rows[x].match_num;
      connectionLocal.query(get_match_data, (err, rows) => {
        if (err) console.error(err);
        else if (rows.length <= 0) {
          // console.log('no data for match??');
          match_deferred.resolve();
        }
        else if (rows.length > 0) {
          let match_num = rows[0].match_num;
          let red_teams = rows[0].red_teams.split(',');
          let blue_teams = rows[0].blue_teams.split(',');
          red_teams.forEach((team, idx, arr) => { arr[idx] = Number(team); });
          blue_teams.forEach((team, idx, arr) => { arr[idx] = Number(team); });
          // console.log(red_teams, blue_teams);
          // console.log(red_teams.indexOf(Number(team_num)), blue_teams.indexOf(Number(team_num)));
          if (red_teams.indexOf(team_num) > -1) {
            // console.log('Red', rows[0].match_num);
            var team_promises = [];
            var max_denied_pts = 0;
            for (var t in blue_teams) {
              let team_deferred = Q.defer();
              // console.log('Opponent', blue_teams[t]);
              let get_team_avg = "SELECT team_num, avg_contributed_offense, " + rows[0].match_num + " AS 'match_num' FROM teams WHERE team_num=" + blue_teams[t];
              connectionLocal.query(get_team_avg, (err, rows) => {
                if (err) console.error(err);
                let get_team_in_match = "SELECT team_num, match_num, tele_deaths, driver_rating, defense_rating, (3*auto_off_level_one+6*auto_off_level_two+2*(auto_hatch_rocket_low_made+auto_hatch_rocket_med_made+auto_hatch_rocket_high_made+auto_hatch_ship_made+tele_hatch_rocket_low_made+tele_hatch_rocket_med_made+tele_hatch_rocket_high_made+tele_hatch_ship_made)+3*(auto_cargo_rocket_low_made+auto_cargo_rocket_med_made+auto_cargo_rocket_high_made+auto_cargo_ship_made+tele_cargo_rocket_low_made+tele_cargo_rocket_med_made+tele_cargo_rocket_high_made+tele_cargo_ship_made)+3*tele_climb_level_one_made+6*(tele_climb_level_two_made+tele_climb_assist_level_two_made)+12*(tele_climb_level_three_made+tele_climb_assist_level_three_made)) AS 'contributed_offense', " + rows[0].avg_contributed_offense + " AS 'avg_contributed_offense' FROM matches WHERE team_num=" + rows[0].team_num + " AND match_num=" + rows[0].match_num;
                connectionLocal.query(get_team_in_match, (err, rows) => {
                  if (err) console.error(err);
                  // console.log('team:', rows[0].team_num);
                  if (rows.length > 0 && rows[0].tele_deaths == 0 && rows[0].driver_rating > 0 && rows[0].defense_rating == 0) {
                    // console.log('relevant data found for team', rows[0].team_num, 'in match', rows[0].match_num);
                    let denied_pts = rows[0].avg_contributed_offense - rows[0].contributed_offense;
                    // console.log('denied_pts:', denied_pts);
                    max_denied_pts += denied_pts > 0 ? denied_pts : 0;
                    // if (denied_pts > max_denied_pts) {
                    //   max_denied_pts = denied_pts;
                      // console.log(denied_pts);
                    // }
                  }
                  // else console.log('no data...', rows[0] ? rows[0].team_num : '');
                  team_deferred.resolve();
                });
              });
              team_promises.push(team_deferred.promise);
            }
            Q.all(team_promises).then(() => {
              // if (max_denied_pts > 0) {
                tot_denied_pts += max_denied_pts;
                count++;
              // }
              // console.log('max_pts:', max_denied_pts);
              match_deferred.resolve(match_num);
              // console.log(match_promises);
            });
          }
          else if (blue_teams.indexOf(team_num) > -1) {
            // console.log('Blue', rows[0].match_num);
            var team_promises = [];
            var max_denied_pts = 0;
            for (var t in red_teams) {
              let team_deferred = Q.defer();
              // console.log('Opponent', red_teams[t]);
              let get_team_avg = "SELECT team_num, avg_contributed_offense, " + rows[0].match_num + " AS 'match_num' FROM teams WHERE team_num=" + red_teams[t];
              connectionLocal.query(get_team_avg, (err, rows) => {
                if (err) console.error(err);
                // console.log('team:', rows[0].team_num);
                let get_team_in_match = "SELECT team_num, match_num, tele_deaths, driver_rating, defense_rating, (3*auto_off_level_one+6*auto_off_level_two+2*(auto_hatch_rocket_low_made+auto_hatch_rocket_med_made+auto_hatch_rocket_high_made+auto_hatch_ship_made+tele_hatch_rocket_low_made+tele_hatch_rocket_med_made+tele_hatch_rocket_high_made+tele_hatch_ship_made)+3*(auto_cargo_rocket_low_made+auto_cargo_rocket_med_made+auto_cargo_rocket_high_made+auto_cargo_ship_made+tele_cargo_rocket_low_made+tele_cargo_rocket_med_made+tele_cargo_rocket_high_made+tele_cargo_ship_made)+3*tele_climb_level_one_made+6*(tele_climb_level_two_made+tele_climb_assist_level_two_made)+12*(tele_climb_level_three_made+tele_climb_assist_level_three_made)) AS 'contributed_offense', " + rows[0].avg_contributed_offense + " AS 'avg_contributed_offense' FROM matches WHERE team_num=" + rows[0].team_num + " AND match_num=" + rows[0].match_num;
                connectionLocal.query(get_team_in_match, (err, rows) => {
                  if (err) console.error(err);
                  if (rows.length > 0 && rows[0].tele_deaths == 0 && rows[0].driver_rating > 0 && rows[0].defense_rating == 0) {
                    // console.log('relevant data found for team', rows[0].team_num, 'in match', rows[0].match_num);
                    let denied_pts = rows[0].avg_contributed_offense - rows[0].contributed_offense;
                    // console.log('denied_pts:', denied_pts);
                    max_denied_pts += denied_pts > 0 ? denied_pts : 0;
                    // if (denied_pts > max_denied_pts) {
                    //   max_denied_pts = denied_pts;
                      // console.log(denied_pts);
                    // }
                  }
                  // else console.log('no data...', rows[0] ? rows[0].team_num : '');
                  team_deferred.resolve();
                });
              });
              team_promises.push(team_deferred.promise);
            }
            Q.all(team_promises).then(() => {
              // if (max_denied_pts > 0) {
                tot_denied_pts += max_denied_pts;
                count++;
              // }
              // console.log('max_pts:', max_denied_pts);
              // console.log('Max for match', rows[0].match_num, '-', max_denied_pts);
              match_deferred.resolve(match_num);
              // console.log(match_promises);
            });
          }
          else {
            console.log(team_num, 'what???');
          }
        }
      });
      match_promises.push(match_deferred.promise);
    }
    //  console.log(team_num, match_promises);
    if (match_promises.length > 0) {
      Q.all(match_promises).then(() => {
        // console.log(team_num, 'done');
        avg_denied_pts = tot_denied_pts / count || 0;
        // console.log('Team:', team_num, 'Count:', count, 'Total:', tot_denied_pts, 'Average:', avg_denied_pts);
        let push_query = "UPDATE teams SET tot_contributed_defense=" + tot_denied_pts + ", avg_contributed_defense=" + avg_denied_pts + " WHERE team_num=" + team_num;
        connectionLocal.query(push_query, (err) => {
          if (err) console.error(err);
          callback();
        });
      });
    }
    else {
      // console.log(team_num, 'empty');
      callback();
    }
  });
}

module.exports = {
  getTeamData: getTeamData,
  updateTeams: updateTeams
}