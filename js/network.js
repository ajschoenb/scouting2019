let os = require('os');
let mysql = require('mysql');
let netmask = require('netmask').Netmask;
let dgram = require('dgram');

let port = 8118; // Random port, anything over a few thousand should be open

var block = null;
var sock = null;
var socket_open = false;
var my_ip = '';

function getNetworkInformation(callback) {
  let netInterfaces = os.networkInterfaces();
  for (var idx in netInterfaces['Wi-Fi']) {
    if (netInterfaces['Wi-Fi'][idx].family == 'IPv4') {
      my_ip = netInterfaces['Wi-Fi'][idx].address;
      block = new netmask(netInterfaces['Wi-Fi'][idx].address, netInterfaces['Wi-Fi'][idx].netmask);
    }
  }
  if (block == null) {
    console.error("Could not find IPv4 WiFi interface");
  }
  callback();
}

function createBroadcastSocket() {
  sock = dgram.createSocket('udp4');
  sock.bind(() => {
    console.log('Broadcasting message to local network');
    socket_open = true;
    sock.setBroadcast(true);
    setInterval(() => { broadcastUDP() }, 1000); // Currently on a 1 second loop, might need to change
    // broadcastUDP();
  });
}

function broadcastUDP() {
  let msg = 'Broadcasting 118 Scouting Server'; //new Buffer("Broadcasting Scouting Server");
  if (socket_open == true && block != null) {
    sock.send(msg, port, block.broadcast, () => {
      // console.log("Sent message:", msg);
    });
  }
}

function createListenerSocket(connectionLocal, callback) {
  sock = dgram.createSocket('udp4');
  sock.on('listening', () => {
    sock.setBroadcast(true);
    console.log('Listening for incoming broadcasts');
  });
  sock.on('message', (msg, rinfo) => {
    // console.log('Received packet from', rinfo.address);
    if (rinfo.address != my_ip) {
      console.log("Connecting to remote DB");
      let connectionRemote = mysql.createConnection({
        multipleStatements: true,
        host     : '127.0.0.1',
        user     : 'root',
        password : '',
        database : 'frcscout2019',
        debug    : false
    });
    connectionRemote.connect((err) => {
      if (err) console.error(err);
      self.configureExpress();
    });
      syncSQL(connectionLocal, connectionRemote, callback);
    }
  });
  sock.bind(port, () => {
    socket_open = true;
  });
}

function syncSQL(connectionLocal, connectionRemote, callback) {
  console.log('Syncing local and remote DB...');
  let clear_sql = "TRUNCATE matches"; // First need to clear the matches out of local DB
  connectionLocal.query(clear_sql, (err, rows) => {
    if (err) console.error(err);
    
    let get_sql = "SELECT * FROM matches"; // Then need to load all the matches in the remote DB
    connectionRemote.query(get_sql, (err, rows) => {
      if (err) console.error(err);
      for (var x in rows) {
        let team_num = Number(rows[x].team_num);
        let match_num = Number(rows[x].match_num);
      
        let auto_off_level_one = Number(rows[x].auto_off_level_one);
        let auto_off_level_two = Number(rows[x].auto_off_level_two);
        let auto_drive_mode = rows[x].auto_drive_mode;
        let auto_start_pos = rows[x].auto_start_pos;
        let auto_hatch_hp_intake_made = Number(rows[x].auto_hatch_hp_intake_made);
        let auto_hatch_hp_intake_missed = Number(rows[x].auto_hatch_hp_intake_missed);
        let auto_hatch_floor_intake_made = Number(rows[x].auto_hatch_floor_intake_made);
        let auto_hatch_floor_intake_missed = Number(rows[x].auto_hatch_floor_intake_missed);
        let auto_cargo_floor_intake_made = Number(rows[x].auto_cargo_floor_intake_made);
        let auto_cargo_floor_intake_missed = Number(rows[x].auto_cargo_floor_intake_missed);
        let auto_cargo_depot_intake_made = Number(rows[x].auto_cargo_depot_intake_made);
        let auto_cargo_depot_intake_missed = Number(rows[x].auto_cargo_depot_intake_missed);
        let auto_hatch_rocket_low_made = Number(rows[x].auto_hatch_rocket_low_made);
        let auto_hatch_rocket_low_missed = Number(rows[x].auto_hatch_rocket_low_missed);
        let auto_hatch_rocket_med_made = Number(rows[x].auto_hatch_rocket_med_made);
        let auto_hatch_rocket_med_missed = Number(rows[x].auto_hatch_rocket_med_missed);
        let auto_hatch_rocket_high_made = Number(rows[x].auto_hatch_rocket_high_made);
        let auto_hatch_rocket_high_missed = Number(rows[x].auto_hatch_rocket_high_missed);
        let auto_hatch_ship_made = Number(rows[x].auto_hatch_ship_made);
        let auto_hatch_ship_missed = Number(rows[x].auto_hatch_ship_missed);
        let auto_hatch_int_drops = Number(rows[x].auto_hatch_int_drops);
        let auto_hatch_knockouts = Number(rows[x].auto_hatch_knockouts);
        let auto_cargo_rocket_low_made = Number(rows[x].auto_cargo_rocket_low_made);
        let auto_cargo_rocket_low_missed = Number(rows[x].auto_cargo_rocket_low_missed);
        let auto_cargo_rocket_med_made = Number(rows[x].auto_cargo_rocket_med_made);
        let auto_cargo_rocket_med_missed = Number(rows[x].auto_cargo_rocket_med_missed);
        let auto_cargo_rocket_high_made = Number(rows[x].auto_cargo_rocket_high_made);
        let auto_cargo_rocket_high_missed = Number(rows[x].auto_cargo_rocket_high_missed);
        let auto_cargo_ship_made = Number(rows[x].auto_cargo_ship_made);
        let auto_cargo_ship_missed = Number(rows[x].auto_cargo_ship_missed);
        let auto_cargo_int_drops = Number(rows[x].auto_cargo_int_drops);
        let auto_cargo_knockouts = Number(rows[x].auto_cargo_knockouts);
        let tele_hatch_hp_intake_made = Number(rows[x].tele_hatch_hp_intake_made);
        let tele_hatch_hp_intake_missed = Number(rows[x].tele_hatch_hp_intake_missed);
        let tele_hatch_floor_intake_made = Number(rows[x].tele_hatch_floor_intake_made);
        let tele_hatch_floor_intake_missed = Number(rows[x].tele_hatch_floor_intake_missed);
        let tele_cargo_hp_intake_made = Number(rows[x].tele_cargo_hp_intake_made);
        let tele_cargo_hp_intake_missed = Number(rows[x].tele_cargo_hp_intake_missed);
        let tele_cargo_floor_intake_made = Number(rows[x].tele_cargo_floor_intake_made);
        let tele_cargo_floor_intake_missed = Number(rows[x].tele_cargo_floor_intake_made);
        let tele_cargo_depot_intake_made = Number(rows[x].tele_cargo_depot_intake_made);
        let tele_cargo_depot_intake_missed = Number(rows[x].tele_cargo_depot_intake_missed);
        let tele_hatch_rocket_low_made = Number(rows[x].tele_hatch_rocket_low_made);
        let tele_hatch_rocket_low_missed = Number(rows[x].tele_hatch_rocket_low_missed);
        let tele_hatch_rocket_med_made = Number(rows[x].tele_hatch_rocket_med_made);
        let tele_hatch_rocket_med_missed = Number(rows[x].tele_hatch_rocket_med_missed);
        let tele_hatch_rocket_high_made = Number(rows[x].tele_hatch_rocket_high_made);
        let tele_hatch_rocket_high_missed = Number(rows[x].tele_hatch_rocket_high_missed);
        let tele_hatch_ship_made = Number(rows[x].tele_hatch_ship_made);
        let tele_hatch_ship_missed = Number(rows[x].tele_hatch_ship_missed);
        let tele_hatch_int_drops = Number(rows[x].tele_hatch_int_drops);
        let tele_hatch_knockouts = Number(rows[x].tele_hatch_knockouts);
        let tele_cargo_rocket_low_made = Number(rows[x].tele_cargo_rocket_low_made);
        let tele_cargo_rocket_low_missed = Number(rows[x].tele_cargo_rocket_low_missed);
        let tele_cargo_rocket_med_made = Number(rows[x].tele_cargo_rocket_med_made);
        let tele_cargo_rocket_med_missed = Number(rows[x].tele_cargo_rocket_med_missed);
        let tele_cargo_rocket_high_made = Number(rows[x].tele_cargo_rocket_high_made);
        let tele_cargo_rocket_high_missed = Number(rows[x].tele_cargo_rocket_high_missed);
        let tele_cargo_ship_made = Number(rows[x].tele_cargo_ship_made);
        let tele_cargo_ship_missed = Number(rows[x].tele_cargo_ship_missed);
        let tele_cargo_int_drops = Number(rows[x].tele_cargo_int_drops);
        let tele_cargo_knockouts = Number(rows[x].tele_cargo_knockouts);
        let tele_climb_level_one_made = Number(rows[x].tele_climb_level_one_made);
        let tele_climb_level_one_missed = Number(rows[x].tele_climb_level_one_missed);
        let tele_climb_level_two_made = Number(rows[x].tele_climb_level_two_made);
        let tele_climb_level_two_missed = Number(rows[x].tele_climb_level_two_missed);
        let tele_climb_level_three_made = Number(rows[x].tele_climb_level_three_made);
        let tele_climb_level_three_missed = Number(rows[x].tele_climb_level_three_missed);
        let tele_climb_start_time = Number(rows[x].tele_climb_start_time);
        let tele_climb_elapsed_time = Number(rows[x].tele_climb_elapsed_time);
        let tele_climb_assist_level_two_made = Number(rows[x].tele_climb_assist_level_two_made);
        let tele_climb_assist_level_two_missed = Number(rows[x].tele_climb_assist_level_two_missed);
        let tele_climb_assist_level_three_made = Number(rows[x].tele_climb_assist_level_three_made);
        let tele_climb_assist_level_three_missed = Number(rows[x].tele_climb_assist_level_three_missed);
        let tele_climb_assisted= Number(rows[x].tele_climb_assisted);
        let driver_rating = Number(rows[x].driver_rating);
        let defense_rating = Number(rows[x].defense_rating);

        let push_sql = "SELECT " + x + " AS 'idx'; INSERT INTO `matches` (`match_num`, `team_num`, `auto_off_level_one`, `auto_off_level_two`, `auto_drive_mode`, `auto_start_pos`, " +
        "`auto_hatch_hp_intake_made`, `auto_hatch_hp_intake_missed`, `auto_hatch_floor_intake_made`, `auto_hatch_floor_intake_missed`, " +
        "`auto_cargo_floor_intake_made`, `auto_cargo_floor_intake_missed`, `auto_cargo_depot_intake_made`, `auto_cargo_depot_intake_missed`, " +
        "`auto_hatch_rocket_low_made`, `auto_hatch_rocket_low_missed`, `auto_hatch_rocket_med_made`, `auto_hatch_rocket_med_missed`, " +
        "`auto_hatch_rocket_high_made`, `auto_hatch_rocket_high_missed`, `auto_hatch_ship_made`, `auto_hatch_ship_missed`, `auto_hatch_int_drops`, " +
        "`auto_hatch_knockouts`, `auto_cargo_rocket_low_made`, `auto_cargo_rocket_low_missed`, `auto_cargo_rocket_med_made`, " +
        "`auto_cargo_rocket_med_missed`, `auto_cargo_rocket_high_made`, `auto_cargo_rocket_high_missed`, `auto_cargo_ship_made`, " +
        "`auto_cargo_ship_missed`, `auto_cargo_int_drops`, `auto_cargo_knockouts`, `tele_hatch_hp_intake_made`, `tele_hatch_hp_intake_missed`, " +
        "`tele_hatch_floor_intake_made`, `tele_hatch_floor_intake_missed`, `tele_cargo_hp_intake_made`, `tele_cargo_hp_intake_missed`, " +
        "`tele_cargo_floor_intake_made`, `tele_cargo_floor_intake_missed`, `tele_cargo_depot_intake_made`, `tele_cargo_depot_intake_missed`, " +
        "`tele_hatch_rocket_low_made`, `tele_hatch_rocket_low_missed`, `tele_hatch_rocket_med_made`, `tele_hatch_rocket_med_missed`, " +
        "`tele_hatch_rocket_high_made`, `tele_hatch_rocket_high_missed`, `tele_hatch_ship_made`, `tele_hatch_ship_missed`, `tele_hatch_int_drops`, " +
        "`tele_hatch_knockouts`, `tele_cargo_rocket_low_made`, `tele_cargo_rocket_low_missed`, `tele_cargo_rocket_med_made`, " +
        "`tele_cargo_rocket_med_missed`, `tele_cargo_rocket_high_made`, `tele_cargo_rocket_high_missed`, `tele_cargo_ship_made`, " +
        "`tele_cargo_ship_missed`, `tele_cargo_int_drops`, `tele_cargo_knockouts`, `tele_climb_level_one_made`, `tele_climb_level_one_missed`, " +
        "`tele_climb_level_two_made`, `tele_climb_level_two_missed`, `tele_climb_level_three_made`, `tele_climb_level_three_missed`, " +
        "`tele_climb_start_time`, `tele_climb_elapsed_time`, `tele_climb_assist_level_two_made`, `tele_climb_assist_level_two_missed`, " +
        "`tele_climb_assist_level_three_made`, `tele_climb_assist_level_three_missed`, `tele_climb_assisted`, `driver_rating`, `defense_rating`) " +
        "VALUES (" + match_num + ", " + team_num + ", " + auto_off_level_one + ", " + auto_off_level_two + ", '" + auto_drive_mode + "', '" + auto_start_pos + "', " +
        auto_hatch_hp_intake_made + ", " + auto_hatch_hp_intake_missed + ", " + auto_hatch_floor_intake_made + ", " + auto_hatch_floor_intake_missed + ", " + 
        auto_cargo_floor_intake_made + ", " + auto_cargo_floor_intake_missed + ", " + auto_cargo_depot_intake_made + ", " + auto_cargo_depot_intake_missed + ", " + 
        auto_hatch_rocket_low_made + ", " + auto_hatch_rocket_low_missed + ", " + auto_hatch_rocket_med_made + ", " + auto_hatch_rocket_med_missed + ", " + 
        auto_hatch_rocket_high_made + ", " + auto_hatch_rocket_high_missed + ", " + auto_hatch_ship_made + ", " + auto_hatch_ship_missed + ", " + 
        auto_hatch_int_drops + ", " + auto_hatch_knockouts + ", " + auto_cargo_rocket_low_made + ", " + auto_cargo_rocket_low_missed + ", " + 
        auto_cargo_rocket_med_made + ", " + auto_cargo_rocket_med_missed + ", " + auto_cargo_rocket_high_made + ", " + auto_cargo_rocket_high_missed + ", " + 
        auto_cargo_ship_made + ", " + auto_cargo_ship_missed + ", " + auto_cargo_int_drops + ", " + auto_cargo_knockouts + ", " + 
        tele_hatch_hp_intake_made + ", " + tele_hatch_hp_intake_missed + ", " + tele_hatch_floor_intake_made + ", " + tele_hatch_floor_intake_missed + ", " + 
        tele_cargo_hp_intake_made + ", " + tele_cargo_hp_intake_missed + ", " + tele_cargo_floor_intake_made + ", " + tele_cargo_floor_intake_missed + ", " + 
        tele_cargo_depot_intake_made + ", " + tele_cargo_depot_intake_missed + ", " + tele_hatch_rocket_low_made + ", " + tele_hatch_rocket_low_missed + ", " + 
        tele_hatch_rocket_med_made + ", " + tele_hatch_rocket_med_missed + ", " + tele_hatch_rocket_high_made + ", " + tele_hatch_rocket_high_missed + ", " + 
        tele_hatch_ship_made + ", " + tele_hatch_ship_missed + ", " + tele_hatch_int_drops + ", " + tele_hatch_knockouts + ", " + tele_cargo_rocket_low_made + ", " + 
        tele_cargo_rocket_low_missed + ", " + tele_cargo_rocket_med_made + ", " + tele_cargo_rocket_med_missed + ", " + tele_cargo_rocket_high_made + ", " +
        tele_cargo_rocket_high_missed + ", " + tele_cargo_ship_made + ", " + tele_cargo_ship_missed + ", " + tele_cargo_int_drops + ", " +
        tele_cargo_knockouts + ", " + tele_climb_level_one_made + ", " + tele_climb_level_one_missed + ", " + tele_climb_level_two_made + ", " +
        tele_climb_level_two_missed + ", " + tele_climb_level_three_made + ", " + tele_climb_level_three_missed + ", " + tele_climb_start_time + ", " +
        tele_climb_elapsed_time + ", " + tele_climb_assist_level_two_made + ", " + tele_climb_assist_level_two_missed + ", " + tele_climb_assist_level_three_made + ", " +
        tele_climb_assist_level_three_missed + ", " + tele_climb_assisted + ", " + driver_rating + ", " + defense_rating + ");";
        connectionLocal.query(push_sql, () => {
          // Possible this logic might not work well, may need to setTimeout(callback)
          if (rows[0].idx >= rows.length - 1) {
            console.log('Synced matches table, need to reload homepage to refresh teams');
            callback();
          }
        });
      }
    });
  });
}

function closeSocket() {
  console.log('Closing network socket');
  socket_open = false;
  sock.close();
}

module.exports = {
  getNetworkInformation: getNetworkInformation,
  createBroadcastSocket: createBroadcastSocket,
  createListenerSocket: createListenerSocket,
  closeSocket: closeSocket
}