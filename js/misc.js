let team = require('./team.js');
let opt = require('./optimize.js');

let https = require('https');
let Q = require('q');

function loadIndexPage(connectionLocal, callback) {
  var team_list = "";
  var score_list = "";
  var rating_list = "";

  let get_teams = "SELECT * FROM teams";
  let promises = [];
  //TEAM QUERY
  connectionLocal.query(get_teams, (err, rows) => {
    if (err) console.error(err);
    for(var x in rows)
    {
      team_list += "<tr class='clickable-row' data-href='/team/"+ rows[x].team_num +"'><td>"+ rows[x].team_num +"</td><td>"+ rows[x].team_name +"</td></tr>";
      let deferred = Q.defer();
      team.updateTeams(connectionLocal, rows[x].team_num, () => {
        deferred.resolve();
        // console.log('resolved');
        // console.log(promises);
      });
      promises.push(deferred.promise);
    }
    // Q.all(promises).then(() => { console.log('done'); });

    Q.all(promises).then(() => {
      // console.log('done');
      //CONTRIB SCORE QUERY
      let get_contrib_score_rank = "SELECT * FROM teams ORDER BY avg_contributed_offense DESC, team_num ASC";
      connectionLocal.query(get_contrib_score_rank, (err, rows) => {
        if (err) console.error(err);
        for(var x in rows)
        {
          score_list += "<tr class='clickable-row' data-href='/team/"+ rows[x].team_num +"'><td>"+ rows[x].team_num +"</td><td>"+ Math.round(1000*(rows[x].avg_auto_hatch_made + rows[x].avg_tele_hatch_made))/1000 +"</td><td>"+ Math.round(1000*(rows[x].avg_auto_cargo_made + rows[x].avg_tele_cargo_made))/1000 + "</td><td>" + rows[x].avg_contributed_offense + "</td><td>" + rows[x].tot_auto_off_level_two + "</td><td>" + rows[x].tot_tele_climb_level_two_made + "</td><td>" + rows[x].tot_tele_climb_level_three_made + "</td><td>" + rows[x].avg_driver_rating + "</td></tr>";
        }
  
        callback({
          team_list: team_list,
          score_list: score_list
        });
      });
    });
  });
}

function loadMatchesPage(connectionLocal, callback) {
  var upcoming_match_list = "";
  var played_match_list = "";
  var matches = [];
  var red_teams = [];
  var blue_teams = [];
  var red_scores = [];
  var blue_scores = [];
  var played = [];
  var num_entered = [];

  let promises = [];
  let get_matches = "SELECT * FROM matches_tba";
  connectionLocal.query(get_matches, (err, rows) => {
    if (err) console.error(err);
    for (var m in rows) {
      matches[rows[m].match_num] = rows[m].match_num;
      red_teams[rows[m].match_num] = rows[m].red_teams;
      blue_teams[rows[m].match_num] = rows[m].blue_teams;

      let deferredR = Q.defer();
      let deferredB = Q.defer();

      let played_query = "SELECT " + rows[m].match_num + " AS 'match_num', '" + rows[m].red_teams + "' AS 'red_teams', '" + rows[m].blue_teams + "' AS 'blue_teams'; SELECT * FROM matches WHERE match_num="  + rows[m].match_num;
      connectionLocal.query(played_query, (err, rows) => {
        if (err) console.error(err);

        let rteams = rows[0][0].red_teams.split(',');
        let bteams = rows[0][0].blue_teams.split(',');

        if (rows[1].length > 1) {
          num_entered[rows[0][0].match_num] = rows[1].length;
          played[rows[0][0].match_num] = true;

          let red_query = "SELECT match_num, SUM(auto_off_level_one) AS 'AuOne', SUM(auto_off_level_two) AS 'AuTwo', " +
          "SUM(auto_hatch_rocket_low_made+auto_hatch_rocket_med_made+auto_hatch_rocket_high_made+auto_hatch_ship_made+tele_hatch_rocket_low_made+tele_hatch_rocket_med_made+tele_hatch_rocket_high_made+tele_hatch_ship_made) AS 'Hatch', " +
          "SUM(auto_cargo_rocket_low_made+auto_cargo_rocket_med_made+auto_cargo_rocket_high_made+auto_cargo_ship_made+tele_cargo_rocket_low_made+tele_cargo_rocket_med_made+tele_cargo_rocket_high_made+tele_cargo_ship_made) AS 'Cargo', " +
          "SUM(tele_climb_level_one_made) AS 'ClOne', SUM(tele_climb_level_two_made+tele_climb_assist_level_two_made) AS 'ClTwo', SUM(tele_climb_level_three_made+tele_climb_assist_level_three_made) AS 'ClThree'" +
          "FROM matches WHERE match_num=" + rows[0][0].match_num + " AND (team_num=" + rteams[0] + " OR team_num=" + rteams[1] + " OR team_num=" + rteams[2] + ")";

          let blue_query = "SELECT match_num, SUM(auto_off_level_one) AS 'AuOne', SUM(auto_off_level_two) AS 'AuTwo', " +
          "SUM(auto_hatch_rocket_low_made+auto_hatch_rocket_med_made+auto_hatch_rocket_high_made+auto_hatch_ship_made+tele_hatch_rocket_low_made+tele_hatch_rocket_med_made+tele_hatch_rocket_high_made+tele_hatch_ship_made) AS 'Hatch', " +
          "SUM(auto_cargo_rocket_low_made+auto_cargo_rocket_med_made+auto_cargo_rocket_high_made+auto_cargo_ship_made+tele_cargo_rocket_low_made+tele_cargo_rocket_med_made+tele_cargo_rocket_high_made+tele_cargo_ship_made) AS 'Cargo', " +
          "SUM(tele_climb_level_one_made) AS 'ClOne', SUM(tele_climb_level_two_made+tele_climb_assist_level_two_made) AS 'ClTwo', SUM(tele_climb_level_three_made+tele_climb_assist_level_three_made) AS 'ClThree'" +
          "FROM matches WHERE match_num=" + rows[0][0].match_num + " AND (team_num=" + bteams[0] + " OR team_num=" + bteams[1] + " OR team_num=" + bteams[2] + ")";

          connectionLocal.query(red_query, (err, rows) => {
            if (err) console.error(err);
            let score = 3*rows[0].AuOne+6*rows[0].AuTwo+2*rows[0].Hatch+3*rows[0].Cargo+3*rows[0].ClOne+6*rows[0].ClTwo+12*rows[0].ClThree;
            // console.log(rows[0].match_num, 'red', score);
            red_scores[rows[0].match_num] = score;
            deferredR.resolve();
          });

          connectionLocal.query(blue_query, (err, rows) => {
            if (err) console.error(err);
            let score = 3*rows[0].AuOne+6*rows[0].AuTwo+2*rows[0].Hatch+3*rows[0].Cargo+3*rows[0].ClOne+6*rows[0].ClTwo+12*rows[0].ClThree;
            // console.log(rows[0].match_num, 'blue', score);
            blue_scores[rows[0].match_num] = score;
            deferredB.resolve();
          });
        }
        else {
          num_entered[rows[0][0].match_num] = 0;
          played[rows[0][0].match_num] = false;

          opt.optimizeAllianceScore(connectionLocal, rteams, [1.0, 1.0, 1.0], (vecR, scoreR) => {
            // console.log(scoreR);
            red_scores[rows[0][0].match_num] = scoreR;
            deferredR.resolve();
          });
          opt.optimizeAllianceScore(connectionLocal, bteams, [1.0, 1.0, 1.0], (vecB, scoreB) => {
            // console.log(scoreB);
            blue_scores[rows[0][0].match_num] = scoreB;
            deferredB.resolve();
          });
        }
      });
      promises.push(deferredR.promise);
      promises.push(deferredB.promise);
    }
    Q.all(promises).then(() => {
      for (var m in matches) {
        if (played[m]) {
          played_match_list += "<tr class='clickablerow' data-href='/match/" + matches[m] + "'><td>" + matches[m] + "</td><td>" + red_teams[m] + "</td><td>" + red_scores[m] + "</td><td>" + blue_teams[m] + "</td><td>" + blue_scores[m] + "</td><td>" + num_entered[m] + "</td><td>" + (played[m] ? "&#10004" : "&#10006") + "</td></tr>";  
        }
        else {
          upcoming_match_list += "<tr class='clickablerow' data-href='/match/" + matches[m] + "'><td>" + matches[m] + "</td><td>" + red_teams[m] + "</td><td>" + red_scores[m] + "</td><td>" + blue_teams[m] + "</td><td>" + blue_scores[m] + "</td><td>" + (played[m] ? "&#10004" : "&#10006") + "</td></tr>";
        }
      }

      callback({
        upcoming_match_list: upcoming_match_list,
        played_match_list: played_match_list
      });
    });
  });
}

function sqlQuery(connectionLocal, req, callback) {
  var sql = req.body.query;
  var query_res = "";
  connectionLocal.query(sql, function(err, rows, fields) {
    if(err)
    {
      console.log(err);
      query_bool = -1;
    }
    else
    {
      query_bool = 1;
      query_res += "<tr>";
      for(var p in rows[0])
      {
        query_res += "<th>" + p + "</th>";
      }
      for(var i in rows)
      {
        query_res += "</tr><tr>";
        for(var p in rows[i])
        {
          query_res += "<td>" + rows[i][p] + "</td>";
        }
        query_res += "</tr>";
      }
    }
    callback(query_res, query_bool);
  });
}

function exportData(connectionLocal, res, fs)
{
  var teams_sql = "SELECT * FROM teams";
  var filename = __dirname + "/../teams.csv";
  var data = "";
  connectionLocal.query(teams_sql, function(err, rows, fields) {
    for(var p in rows[0])
    {
      data += p + ", ";
    }
    data = data.slice(0, data.length - 2); // Remove the extra comma
    data += "\n";
    for(var i in rows)
    {
      for(var p in rows[i])
      {
        data += rows[i][p] + ", ";
      }
      data = data.slice(0, data.length - 2); // Remove the extra comma
      data += "\n";
    }
    // console.log(data);
    fs.writeFile(filename, data, function(err) {
      console.log(err ? err : "File saved to " + __dirname + "\\..");
      res.download(__dirname + "/../teams.csv");
    });
  });
}

function getEvents(tba, req, callback)
{
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth() + 1;
  var date_str = "2018-" + month + "-" + day;
  var events = "";
  tba.getEventList((err, list_of_teams) => {
    if (err) console.error(err);
    for(var x in list_of_teams) {
      var event_date = list_of_teams[x].start_date.split("-");
      events += "<option>" + list_of_teams[x].event_code + "-" + list_of_teams[x].name + "</option>\n";
    }
    
    callback({
      req: req,
      events: events
    });
  });
}

function loadEvent(connectionLocal, tba, req, callback) {
  var event_code = req.body.event.split("-")[0];
  var teams = [];
  tba.getEventTeams(event_code, 2019, (err, team_list) => {
    if (err) console.error(err);
    var drop_teams_sql = "TRUNCATE `teams`"
    var drop_matches_sql = "TRUNCATE `matches`"
    connectionLocal.query(drop_teams_sql, function(err) {
      if(err) console.error(err);
      connectionLocal.query(drop_matches_sql, function(err) {
        if(err) console.error(err);
        
        var promises = [];
        for(var x in team_list) {
          let deferred = Q.defer();
          var team_sql = "INSERT INTO teams (team_num, team_name) VALUES (" + team_list[x].team_number + ", \"" + team_list[x].nickname + "\")";
          connectionLocal.query(team_sql, function(err) {
            if(err) console.log(err);
            deferred.resolve();
          });
          promises.push(deferred.promise);
        }

        Q.all(promises).then(() => { loadMatches(connectionLocal, tba, event_code, () => { callback(); }); });
      });
    });
  });
}

function loadMatches(connectionLocal, tba, event_code, callback) {
  tba.getEventMatches(event_code, 2019, (err, matches) => {
    if (err) console.error(err);
    matches.sort((a, b) => { return a.match_number - b.match_number; });
    let match_promises = [];
    let drop_matches = "TRUNCATE matches_tba";
    connectionLocal.query(drop_matches, (err) => {
      if (err) console.error(err);
      for (var x in matches) {
        if (matches[x].comp_level == 'qm') {
          let red_teams = matches[x].alliances.red.teams;
          let blue_teams = matches[x].alliances.blue.teams;
          red_teams.forEach((team, idx, arr) => { arr[idx] = team.slice(3); });
          blue_teams.forEach((team, idx, arr) => { arr[idx] = team.slice(3); });
          // console.log(red_teams, blue_teams);
          let push_teams = "INSERT INTO matches_tba (match_num, red_teams, blue_teams) VALUES (" + matches[x].match_number + ", '" + red_teams.toString() + "', '" + blue_teams.toString() + "');";
          let deferred = Q.defer();
          connectionLocal.query(push_teams, (err) => {
            if (err) console.error(err);
            deferred.resolve();
          });
          match_promises.push(deferred.promise);
        }
      }
      Q.all(match_promises).then(() => {
        callback();
      });
    });
  });
}

function callTBA(request, key) {
  return new Promise((resolve) => {
    https.request({
      protocol: "https:",
      host: "www.thebluealliance.com",
      path: `/api/v3${request}`,
      headers: {
        "X-TBA-Auth-Key": key
      }
    },(res) => {
      var data = "";
      res.on("data",function(d) {
        data += String(d);
      });
      res.on("end",function() {
        resolve(JSON.parse(data));
      });
    }).end();
  });
}

module.exports = {
  callTBA: callTBA,
  loadIndexPage: loadIndexPage,
  loadMatchesPage: loadMatchesPage,
  sqlQuery: sqlQuery,
  exportData: exportData,
  getEvents: getEvents,
  loadEvent: loadEvent,
  loadMatches: loadMatches
}