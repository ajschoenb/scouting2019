

// Function to determine the probability that blue wins a match given the six teams playing
function getMatchProbs(blue_teams, red_teams, connectionLocal, callback)
{
  var x1, s1, n1, x2, s2, n2;
  var crossX1, crossS1, climbX1, climbS1, plusX1, plusS1, exchX1, exchS1;
  var crossX2, crossS2, climbX2, climbS2, plusX2, plusS2, exchX2, exchS2;
  var auto_prob1, auto_prob2, climb_prob1, climb_prob2;
  var blue_query = "SELECT AVG(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'avg', STDDEV(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'stdev', " +
      "COUNT(tele_scale_high_made)/3 AS 'size', AVG(auto_cross) AS 'cross_avg', STDDEV(auto_cross) AS 'cross_std', 3*AVG(tele_climb) AS 'climb_avg', STDDEV(tele_climb) as 'climb_std', 3*AVG(tele_plus_one+2*tele_plus_two) AS 'plus_avg', STDDEV(tele_plus_one+2*tele_plus_two) AS 'plus_std', 3*AVG(tele_exchange_made) AS 'exch_avg', STDDEV(tele_exchange_made) AS 'exch_std' FROM matches WHERE team_num=" + blue_teams[0] + " OR team_num=" + 
      blue_teams[1] + " OR team_num=" + blue_teams[2];
  var red_query = "SELECT AVG(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'avg', STDDEV(1.5*auto_scale_high_made+1.5*auto_scale_low_made+1.5*auto_near_switch_made+tele_scale_high_made+tele_scale_low_made+tele_near_switch_made+tele_far_switch_made+tele_exchange_made+10*tele_climb+10*tele_plus_one+tele_platform) AS 'stdev', " +
  "COUNT(tele_scale_high_made)/3 AS 'size', AVG(auto_cross) AS 'cross_avg', STDDEV(auto_cross) AS 'cross_std', 3*AVG(tele_climb) AS 'climb_avg', STDDEV(tele_climb) as 'climb_std', 3*AVG(tele_plus_one+2*tele_plus_two) AS 'plus_avg', STDDEV(tele_plus_one+2*tele_plus_two) AS 'plus_std', 3*AVG(tele_exchange_made) AS 'exch_avg', STDDEV(tele_exchange_made) AS 'exch_std' FROM matches WHERE team_num=" + red_teams[0] + " OR team_num=" + 
    red_teams[1] + " OR team_num=" + red_teams[2];
  connectionLocal.query(blue_query, (err, rows) => {
    if(err) console.error(err);
    x1 = rows[0].avg;
    s1 = rows[0].stdev;
    n1 = rows[0].size;
    // console.log('blue:', rows);

    crossX1 = rows[0].cross_avg;
    crossS1 = rows[0].cross_std;
    climbX1 = rows[0].climb_avg;
    climbS1 = rows[0].climb_std;
    plusX1 = rows[0].plus_avg;
    plusS1 = rows[0].plus_std;
    exchX1 = rows[0].exch_avg;
    exchS1 = rows[0].exch_std;

    connectionLocal.query(red_query, (err, rows) => {
      if(err) console.error(err);
      x2 = rows[0].avg;
      s2 = rows[0].stdev;
      n2 = rows[0].size;
      // console.log('red:', rows);

      crossX2 = rows[0].cross_avg;
      crossS2 = rows[0].cross_std;
      climbX2 = rows[0].climb_avg;
      climbS2 = rows[0].climb_std;
      plusX2 = rows[0].plus_avg;
      plusS2 = rows[0].plus_std;
      exchX2 = rows[0].exch_avg;
      exchS2 = rows[0].exch_std;

      // console.log(x1, s1, n1, x2, s2, n2);
      var pred_win = welchTTest(x1, s1, n1, x2, s2, n2);
      auto_prob1 = Math.pow(crossX1, 3);
      auto_prob2 = Math.pow(crossX2, 3);

      climb_prob1 = climbX1 / 3 * plusX1 / 3 + (1 - normCDF(3, exchX1, exchS1));
      climb_prob2 = climbX2 / 3 * plusX2 / 3 + (1 - normCDF(3, exchX2, exchS2));

      var blue_rp = pred_win * 2 + auto_prob1 + climb_prob1;
      var red_rp = (1 - pred_win) * 2 + auto_prob2 + climb_prob2;

      callback({ 
        blue_win: pred_win,
        red_win: 1 - pred_win,
        blue_rp: blue_rp,
        red_rp: red_rp
      });
    });
  });
}

// Overall Welch's t-test function to determine p-value from team/alliance data
function welchTTest(x1, s1, n1, x2, s2, n2)
{
  let t = welchTScore(x1, s1, n1, x2, s2, n2);
  let v = welchDOF(s1, n1, s2, n2);
  let p = tCDF(t, v);
  return p;
}

// Welch's t-test function to generate t-score from team/alliance data
function welchTScore(x1, s1, n1, x2, s2, n2)
{
  let ret = (x1 - x2) / Math.sqrt((s1 * s1 / n1) + (s2 * s2 / n2));
  return ret;
}

// Determine the degree of freedom for Welch's t-test using the Welch-Satterthwaite equation
function welchDOF(s1, n1, s2, n2)
{
  let v1 = n1 - 1;
  let v2 = n2 - 1;
  let ret = (Math.pow((s1 * s1 / n1) + (s2 * s2 / n2), 2) / ((Math.pow(s1, 4) / (n1 * n1 * v1)) + (Math.pow(s2, 4) / (n2 * n2 * v2))));
  return ret;
}

// CDF function for the Student t-distribution
function tCDF(x, dof)
{
  var dof2 = dof / 2;
  return ibeta((x + Math.sqrt(x * x + dof)) / (2 * Math.sqrt(x * x + dof)), dof2, dof2);  
}

// CDF function for the normal distribution
function normCDF(x, mean, std)
{
  return 0.5 * (1 + erf((x - mean) / Math.sqrt(2 * std * std)));
}

// Returns the error function erf(x)
function erf(x) {
  var cof = [-1.3026537197817094, 6.4196979235649026e-1, 1.9476473204185836e-2,
             -9.561514786808631e-3, -9.46595344482036e-4, 3.66839497852761e-4,
             4.2523324806907e-5, -2.0278578112534e-5, -1.624290004647e-6,
             1.303655835580e-6, 1.5626441722e-8, -8.5238095915e-8,
             6.529054439e-9, 5.059343495e-9, -9.91364156e-10,
             -2.27365122e-10, 9.6467911e-11, 2.394038e-12,
             -6.886027e-12, 8.94487e-13, 3.13092e-13,
             -1.12708e-13, 3.81e-16, 7.106e-15,
             -1.523e-15, -9.4e-17, 1.21e-16,
             -2.8e-17];
  var j = cof.length - 1;
  var isneg = false;
  var d = 0;
  var dd = 0;
  var t, ty, tmp, res;

  if (x < 0) {
    x = -x;
    isneg = true;
  }

  t = 2 / (2 + x);
  ty = 4 * t - 2;

  for(; j > 0; j--) {
    tmp = d;
    d = ty * d - dd + cof[j];
    dd = tmp;
  }

  res = t * Math.exp(-x * x + 0.5 * (cof[0] + ty * d) - dd);
  return isneg ? res - 1 : 1 - res;
}

// Returns the incomplete beta function I_x(a,b)
function ibeta(x, a, b)
{
  // Factors in front of the continued fraction.
  var bt = (x === 0 || x === 1) ?  0 :
    Math.exp(gammaln(a + b) - gammaln(a) -
             gammaln(b) + a * Math.log(x) + b *
             Math.log(1 - x));
  if (x < 0 || x > 1)
    return false;
  if (x < (a + 1) / (a + b + 2))
    // Use continued fraction directly.
    return bt * betacf(x, a, b) / a;
  // else use continued fraction after making the symmetry transformation.
  return 1 - bt * betacf(1 - x, b, a) / b;
}

// Log-gamma function
function gammaln(x)
{
  var j = 0;
  var cof = [
    76.18009172947146, -86.50532032941677, 24.01409824083091,
    -1.231739572450155, 0.1208650973866179e-2, -0.5395239384953e-5
  ];
  var ser = 1.000000000190015;
  var xx, y, tmp;
  tmp = (y = xx = x) + 5.5;
  tmp -= (xx + 0.5) * Math.log(tmp);
  for (; j < 6; j++)
    ser += cof[j] / ++y;
  return Math.log(2.5066282746310005 * ser / xx) - tmp;
}

// Evaluates the continued fraction for incomplete beta function by modified Lentz's method.
betacf = function betacf(x, a, b)
{
  var fpmin = 1e-30;
  var m = 1;
  var qab = a + b;
  var qap = a + 1;
  var qam = a - 1;
  var c = 1;
  var d = 1 - qab * x / qap;
  var m2, aa, del, h;

  // These q's will be used in factors that occur in the coefficients
  if (Math.abs(d) < fpmin)
    d = fpmin;
  d = 1 / d;
  h = d;

  for (; m <= 100; m++)
  {
    m2 = 2 * m;
    aa = m * (b - m) * x / ((qam + m2) * (a + m2));
    // One step (the even one) of the recurrence
    d = 1 + aa * d;
    if (Math.abs(d) < fpmin)
      d = fpmin;
    c = 1 + aa / c;
    if (Math.abs(c) < fpmin)
      c = fpmin;
    d = 1 / d;
    h *= d * c;
    aa = -(a + m) * (qab + m) * x / ((a + m2) * (qap + m2));
    // Next step of the recurrence (the odd one)
    d = 1 + aa * d;
    if (Math.abs(d) < fpmin)
      d = fpmin;
    c = 1 + aa / c;
    if (Math.abs(c) < fpmin)
      c = fpmin;
    d = 1 / d;
    del = d * c;
    h *= del;
    if (Math.abs(del - 1.0) < 3e-7)
      break;
  }

  return h;
}

module.exports = {
  getMatchProbs: getMatchProbs,
  tCDF: tCDF,
  normCDF: normCDF,
  welchTTest: welchTTest,
  welchTScore: welchTScore,
  welchDOF: welchDOF
}