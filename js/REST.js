//where all the page connections happen and back-end javascript
let team = require('./team.js');
let data_entry = require('./data_entry.js');
let misc = require('./misc.js');
let stat = require('./stat.js');
let optm = require('./optimize.js');
let net = require('./network.js');

let fs = require("fs");
let TBA = require('thebluealliance');
let tba = new TBA('FRCScout2018','Software for scouting for 2018 season','1.1.1');
let message = "";

function REST_ROUTER(router, connectionLocal)
{
    var self = this;
    self.handleRoutes(router, connectionLocal);
}

REST_ROUTER.prototype.handleRoutes = function(router, connectionLocal)
{
  var most_recent = 0;
  var most_recent_match = 0;
  var num_matches = 0;
  var query_bool = 0;
  var query_res = "";

  router.get('/driver-entry', (req, res) => {
    res.render('pages/driver_entry');
  });

  router.post('/parse-driver', (req, res) => {
    data_entry.parseDriver(connectionLocal, req, () => {
      res.redirect('/driver-entry');
    });
  });

  router.get("/api/team/:team_num", (req, res) => {
    var query = "SELECT * FROM teams WHERE team_num=" + req.params.team_num;
    connectionLocal.query(query, function(err, rows) {
      if(err) console.log(err);
      res.send(rows);
    });
  });

  router.get('/api/match/:team_1,:team_2,:team_3,:team_4,:team_5,:team_6', (req, res) => {
    stat.getMatchProbs([req.params.team_1, req.params.team_2, req.params.team_3], [req.params.team_4, req.params.team_5, req.params.team_6], connectionLocal, (data) => {
      res.send(data);
    });
  });

  router.get('/optimize/:team_1,:team_2,:team_3', (req, res) => {
    optm.optimizeAllianceScore(connectionLocal, [req.params.team_1, req.params.team_2, req.params.team_3], [1,1,1], (vec, score) => {
      res.send(vec + "\n" + score);
    });
  });

  router.get('/netsync', (req, res) => {
    res.render('pages/netsync');
  });

  router.post('/netbroadcast', (req, res) => {
    net.getNetworkInformation(() => {
      net.createBroadcastSocket();
      setTimeout(net.closeSocket, 10000); // Arbitrarily shutting down the socket after 10 sec of pulsing
      res.redirect('/netsync');
    });
  });

  router.post('/netlisten', (req, res) => {
    net.createListenerSocket(connectionLocal, () => {
      net.closeSocket();
      res.redirect('/netsync');
    });
  });

  router.get('/', (req, res) => {
    misc.loadIndexPage(connectionLocal, (dat) => {
      res.render('pages/index', dat);
    });
  });

  router.get('/matches', (req, res) => {
    misc.loadMatchesPage(connectionLocal, (dat) => {
      res.render('pages/matches', dat);
    });
  });

  router.get("/sql", (req, res) => {
    var message = "";
    if(query_bool == -1)
      message = "<div class=\"alert alert-danger\" role=\"alert\"><p><b>Oh snap</b>, looks like there's a mistake in your query. Data not queried.</p></div>";
    else if(query_bool != -1 && query_bool != 0)
      message = "<div class=\"alert alert-success\" role=\"alert\"><p>Data has been <b>successfully</b> queried.</p></div>";
    else if(query_bool == 1)
    {
      query_res = "";
      query_bool = 0;
    }
    
    res.render("pages/sql", {
      req: req,
      message: message,
      result: query_res
    });
  });

  router.post("/query", (req, res) => {
    misc.sqlQuery(connectionLocal, req, (result, bool) => {
      query_res = result;
      query_bool = bool;
      res.redirect("/sql");
    });
  });

  router.get("/export", (req, res) => {
    misc.exportData(connectionLocal, res, fs);
  });

  // router.get("/matches", (req, res) => {
  //   misc.loadMatches(connectionLocal, tba, req, () => {
  //     res.send("done");
  //   });
  // })

  router.get("/event", (req, res) => {
    misc.getEvents(tba, req, (dat) => {
      res.render("pages/event", dat);
    });
  });

  router.post("/parse-event", (req, res) => {
    misc.loadEvent(connectionLocal, tba, req, () => {
      res.redirect("/");
    });
  });

  router.get("/alliance-gen", (req, res) => {
    res.render("pages/alliance_gen", { req: req });
  });

  router.post("/alliance-gen", (req, res) => {
    var team_1 = Number(req.body.team_1);
    var team_2 = Number(req.body.team_2);
    var team_3 = Number(req.body.team_3);
    res.redirect("/alliance/" + team_1 + "," + team_2 + "," + team_3);
  });

  router.get('/alliance/:team_1,:team_2,:team_3', (req, res) => {
    var team_num_1 = !Number.isNaN(req.params.team_1) ? Number(req.params.team_1) : 0;
    var team_num_2 = !Number.isNaN(req.params.team_2) ? Number(req.params.team_2) : 0;
    var team_num_3 = !Number.isNaN(req.params.team_3) ? Number(req.params.team_3) : 0;

    team.getTeamData(connectionLocal, req, team_num_1, (dat_1) => {
      team.getTeamData(connectionLocal, req, team_num_2, (dat_2) => {
        team.getTeamData(connectionLocal, req, team_num_3, (dat_3) => {
          optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [1.0, 1.0, 1.0], (vec1, score_ovr) => {
            optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [0.5, 1.0, 1.0], (vec2, score50_1) => {
              optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [1.0, 0.5, 1.0], (vec3, score50_2) => {
                optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [1.0, 1.0, 0.5], (vec4, score50_3) => {
                  optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [0, 1.0, 1.0], (vec5, score0_1) => {
                    optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [1.0, 0, 1.0], (vec6, score0_2) => {
                      optm.optimizeAllianceScore(connectionLocal, [ team_num_1, team_num_2, team_num_3 ], [1.0, 1.0, 0], (vec7, score0_3) => {
                        res.render('pages/alliance', {
                          team_1: dat_1,
                          team_2: dat_2,
                          team_3: dat_3,
                          opt_alliance_score: score_ovr,
                          pt_swing_50_1: Math.round(1000 * Math.max(score_ovr - score50_1, 0)) / 1000,
                          pt_swing_50_2: Math.round(1000 * Math.max(score_ovr - score50_2, 0)) / 1000,
                          pt_swing_50_3: Math.round(1000 * Math.max(score_ovr - score50_3, 0)) / 1000,
                          pt_swing_0_1: Math.round(1000 * Math.max(score_ovr - score0_1, 0)) / 1000,
                          pt_swing_0_2: Math.round(1000 * Math.max(score_ovr - score0_2, 0)) / 1000,
                          pt_swing_0_3: Math.round(1000 * Math.max(score_ovr - score0_3, 0)) / 1000
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
	});

  router.get('/team/:team_num', (req,res) => {
    team.getTeamData(connectionLocal, req, req.params.team_num, (dat) => {
      if(dat.skip_render) {
        res.redirect("/");
      }
      else {
        res.render('pages/team', dat);
      }
    });
  });

  router.get('/data-entry', (req, res) => {
    var display_entry = "";
    if(most_recent == -1)
      display_entry = '<div class="alert alert-danger" role="alert"><p><b>Oh snap</b>, looks like this may be a duplicate entry. Data not entered.</p></div>';
    else if(most_recent != -1 && most_recent != 0)
      display_entry = "<div class=\"alert alert-success\" role=\"alert\"><p>Data for <b>"+ most_recent +"</b> has been <b>successfully</b> entered. <b>" + num_matches + " teams</b> have been entered for <b>match #" + most_recent_match + ".</b></p></div>";

    res.render('pages/data_entry', {
      req: req,
      message: display_entry
    });
  });

  router.post('/parse-data', (req, res) => {
    data_entry.parseData(connectionLocal, req, (recent, recent_match, num) => {
      most_recent = recent;
      most_recent_match = recent_match;
      num_matches = num;
      res.redirect('/data-entry');
    });
  });
}

module.exports = REST_ROUTER;
