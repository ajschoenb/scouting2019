// server.js

let mysql = require('mysql');
let rest = require("./js/REST.js");
let morgan = require("morgan");
let favicon = require('serve-favicon');
let express = require('express');
let bodyParser = require("body-parser"); // Body parser for fetch posted data
let app = express();
var connectionLocal = null;

app.set('view engine', 'ejs');

app.use(express.static("public"));

function REST()
{
    var self = this;
    self.connectMysql();
}

REST.prototype.connectMysql = function()
{
    var self = this;

    var poolLocal = null;

    console.log("Connecting to local DB");
    connectionLocal = mysql.createPool({
        multipleStatements: true,
        connectionLimit: 75,
        host     : '127.0.0.1',
        user     : 'root',
        password : '',
        database : 'frcscout2019',
        debug    : false
    });
    // connectionLocal.connect((err) => {
    //   if (err) console.error(err);
      self.configureExpress();
    // });
}

REST.prototype.configureExpress = function()
{
    var self = this;
    app.use(favicon('public/images/favicon.ico'));
    app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(morgan("dev"));
    // app.use(multer({dest: "public/images"}));
    var router = express.Router();
    app.use('/', router);
    var rest_router = new rest(router, connectionLocal);
    self.startServer();
}

REST.prototype.startServer = function()
{
    var port = Number(process.env.PORT || 8000);
    app.listen(port, function() {
        console.log('FRC Scout servers running on port 8000');
    });
}

REST.prototype.stop = function(err)
{
    console.log("MYSQL ERROR THROWN: \n" + err);
    process.exit(1);
}

new REST();
